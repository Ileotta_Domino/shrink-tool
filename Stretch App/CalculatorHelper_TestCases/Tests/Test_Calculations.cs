﻿using Ninject;
using Ninject.MockingKernel.Moq;
using NUnit.Framework;
using StretchApp.Helper;
using StretchApp.Helper.Interfaces;
using StretchApp.Parameters.Interfaces;
using System;
using System.Collections.Generic;
using System.Text;

namespace CalculatorHelper_TestCases.Tests
{
    public class Test_Calculations
    {
        private readonly MoqMockingKernel _kernel;
        public Test_Calculations()
        {
            _kernel = new MoqMockingKernel();
            _kernel.Bind<ICalculatorHelper>().To<CalculatorHelper>();
        }

        [SetUp]
        public void SetUp()
        {
            _kernel.Reset();
        }
        [Test]

        [TestCase(100, 50, 50)]
        [TestCase(100, 100, 0)]
        [TestCase(100, 0, 100)]
        public void Test_CalulculatePercentage(int part, int total, int res)
        {
            //setup the mock
            var inputProcessor_Mock = _kernel.GetMock<IinputProcessor>();
            var tupleProcessor_Mock = _kernel.GetMock<ITupleProcessor>();

            var _calculatorHelper = _kernel.Get<ICalculatorHelper>();
            var result = _calculatorHelper.CalulculatePercentage(part, total);
            Assert.AreEqual(res,result);

        }
        [TestCase(100, 50, 50)]
        [TestCase(100, 100, 0)]
        [TestCase(100, 0, 100)]
        public void Test_CalulculateStartLocationPercentage(int part, int total, int res)
        {
            //setup the mock
            var inputProcessor_Mock = _kernel.GetMock<IinputProcessor>();
            var tupleProcessor_Mock = _kernel.GetMock<ITupleProcessor>();

            var _calculatorHelper = _kernel.Get<ICalculatorHelper>();
            var result = _calculatorHelper.CalulculatePercentage(part, total);
            Assert.AreEqual(res, result);

        }
    }
}
