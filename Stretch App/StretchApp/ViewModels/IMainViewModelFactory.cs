﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace StretchApp.ViewModels
{
    public interface IMainViewModelFactory
    {
        IMainViewModel CreateViewPersonViewModel([parameterType parameterName..])
    }
}
