﻿using StretchApp.Helper;
using StretchApp.Helper.Interfaces;
using StretchApp.Parameters.Interfaces;
using StretchApp.Views;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Diagnostics;
using System.IO;

namespace StretchApp.ViewModels
{
    class ViewModelAboutPage : ViewModelBase, IClosableViewModel
    {

        private readonly IinputProcessor _inputProcessor;
        private readonly ITupleProcessor _tupleProcessor;
        private readonly ICalculatorHelper _calculatorHelper;
        private readonly IConfigurationListHelper _configurationListHelper;

        public string ReleasedTo { get; set; }
        public RelayCommand ClosePageCommand { get; set; }
        public RelayCommand GoOpenInstructionsCommand { get; set; }
        public event EventHandler CloseWindowEvent;
        public ViewModelAboutPage(IinputProcessor inputProcessor, ICalculatorHelper calculatorHelper, ITupleProcessor tupleProcessor, IConfigurationListHelper configurationListHelper)
        {
            _inputProcessor = inputProcessor;
            _calculatorHelper = calculatorHelper;
            _tupleProcessor = tupleProcessor;
            _configurationListHelper = configurationListHelper;

            ReleasedTo = _inputProcessor.GetReleasedToValueValue();
            ClosePageCommand = new RelayCommand(ClosePage, canexecute);
            GoOpenInstructionsCommand = new RelayCommand(OpenInstruction, canexecute);
            //   ClosePageCommand = new RelayCommand(CloseWin);
        }
        private bool canexecute(object parameter)
        {
            return true;
        }

        
         private void OpenInstruction(object parameter)
        {
            Process wordProcess = new Process();
       //     MessageBox.Show("Environment.CurrentDirectory");
            string gesturefile = Path.Combine(Environment.CurrentDirectory, @"Shrink_App_User.docx");
            wordProcess.StartInfo.FileName = gesturefile;
            wordProcess.StartInfo.UseShellExecute = true;
            wordProcess.Start();

        }
        private void ClosePage(object parameter)
        {
            CloseWindowEvent(this, null);

        }

    }
}
