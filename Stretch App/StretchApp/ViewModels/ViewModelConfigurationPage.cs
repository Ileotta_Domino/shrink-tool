﻿using Microsoft.Win32;
using StretchApp.Helper;
using StretchApp.Helper.Interfaces;
using StretchApp.Parameters.Interfaces;
using StretchApp.Views;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;

namespace StretchApp.ViewModels
{
    class ViewModelConfigurationPage : ViewModelBase, IClosableViewModel
    {

        private readonly IinputProcessor _inputProcessor;
        private readonly ITupleProcessor _tupleProcessor;
        private readonly ICalculatorHelper _calculatorHelper;
        private readonly IConfigurationListHelper _configurationListHelper;

        public RelayCommand SaveFileCommand { get; set; }
        public RelayCommand LoadFileCommand { get; set; }
        public RelayCommand ClosePageCommand { get; set; }

        public event EventHandler CloseWindowEvent;
        private string _fileToSave;
        public String FileToSave
        {
            get { return _fileToSave; }

            set
            {
                this._fileToSave = value;
                _inputProcessor.SetConfigurationFileValue(FileToSave);
                RaisePropertyChanged("FileNamefromGui");
            }
        }


        public ViewModelConfigurationPage( IinputProcessor inputProcessor, ICalculatorHelper calculatorHelper,  ITupleProcessor tupleProcessor, IConfigurationListHelper configurationListHelper)
        {
            _inputProcessor = inputProcessor;
            _calculatorHelper = calculatorHelper;
            _tupleProcessor = tupleProcessor;
            _configurationListHelper = configurationListHelper;

            SaveFileCommand = new RelayCommand(SavePageStart, canexecute);
            LoadFileCommand = new RelayCommand(LoadPageStart, canexecute);
            ClosePageCommand = new RelayCommand(ClosePageStart, canexecute);
            //   ClosePageCommand = new RelayCommand(CloseWin);
        }

        private bool canexecute(object parameter)
        {
            return true;
        }
        private void SavePageStart(object parameter)
        {
            SaveFileDialog saveFileDialog = new Microsoft.Win32.SaveFileDialog();
            string folderPath = saveFileDialog.FileName;
            saveFileDialog.FileName = _inputProcessor.GetConfigurationFileValue();  //  "Document"; // Default file name
            saveFileDialog.DefaultExt = ".text"; // Default file extension
            saveFileDialog.Filter = "Text documents (.txt)|*.txt"; // Filter files by extensionSaveFileDialog saveFileDialog = new Microsoft.Win32.SaveFileDialog();
            saveFileDialog.RestoreDirectory = true;

            Nullable<bool> result = saveFileDialog.ShowDialog();
            if (result == true)
            {
                 folderPath = saveFileDialog.FileName;
                //      string folderPath = new DirectoryInfo(saveFileDialog.FileName).Name;
                //    File.WriteAllText(Path.GetDirectoryName(saveFileDialog.FileName), "");
                //   string filename = saveFileDialog.FileName;
                _configurationListHelper.WriteActualConfiguration();
                _configurationListHelper.WriteListTofile(folderPath);
            }
            //   CloseWindowEvent(this, null);
        }



        private void LoadPageStart(object parameter)
        {
            // Create OpenFileDialog 
            Microsoft.Win32.OpenFileDialog dlg = new Microsoft.Win32.OpenFileDialog();

            dlg.DefaultExt = ".txt";

            // Display OpenFileDialog by calling ShowDialog method 
            Nullable<bool> result = dlg.ShowDialog();


            // Get the selected file name and display in a TextBox 
            if (result == true)
            {
                // Open document 
                FileToSave = dlg.FileName;
                //  FileName.Text = FileName;
                _configurationListHelper.ReadConfigurationFile(FileToSave);

            }

        }
        public void CloseWin(object obj)
        {
            Window win = obj as Window;
            win.Close();
        }

        private void ClosePageStart(object parameter)
        {
            var win = new CalculatorPage { DataContext = new ViewModelCalculatorPage(_calculatorHelper, _inputProcessor, _tupleProcessor, _configurationListHelper) };
            win.Show();
            CloseWindowEvent(this, null);
            CloseWindow();

        }

    }

}
