﻿using StretchApp.Helper;
using StretchApp.Helper.Interfaces;
using StretchApp.Parameters.Interfaces;
using StretchApp.Views;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.IO;
using System.Linq;
using System.Security;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Input;

namespace StretchApp.ViewModels
{
    class MainViewModel : ViewModelBase, IClosableViewModel, INotifyPropertyChanged
    {
        private readonly IEncrypterHelper _encrypterHelper;
        private readonly ICalculatorHelper _calculatorHelper;
        public readonly IinputProcessor _inputProcessor;
        private readonly ISerialNumberHelper _serialNumberHelper;
        private readonly IFileEncrypterHelper _fileEncrypterHelper;
        private readonly ITupleProcessor _tupleProcessor;
        private readonly IConfigurationListHelper _configurationListHelper;
        private string _PasswordInVM;
        private string pwf = @"c:\Domino\Shrink\Key\hlp";
        public event PropertyChangedEventHandler PropertyChanged;
        public event EventHandler CloseWindowEvent;
        private bool identified = false;
        public string Password { get; set; }


        public SecureString OriginalPassword { get; set; }

        /// <summary>
        /// 
        /// </summary>
        public string PasswordInVM
        {
            get
            {
                return _PasswordInVM;
            }
            set
            {
                _PasswordInVM = value;
                OnPropertyChanged("PasswordInVM");
            }
        }

        public RelayCommand LoginCommand { get; set; }
        public RelayCommand GoToCalculatorPageCommand { get; set; }
        public RelayCommand GoToIdentifierPageCommand { get; set; }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="encrypterHelper"></param>
        /// <param name="inputProcessor"></param>
        /// <param name="calculatorHelper"></param>
        /// <param name="serialNumberHelper"></param>
        /// <param name="fileEncrypterHelper"></param>
        /// <param name="tupleProcessor"></param>
        public MainViewModel(IEncrypterHelper encrypterHelper, IinputProcessor inputProcessor, ICalculatorHelper calculatorHelper, ISerialNumberHelper serialNumberHelper, IFileEncrypterHelper fileEncrypterHelper, ITupleProcessor tupleProcessor, IConfigurationListHelper configurationListHelper)
        {
            _encrypterHelper = encrypterHelper;
            _inputProcessor = inputProcessor;
            _calculatorHelper = calculatorHelper;
            _serialNumberHelper = serialNumberHelper;
            _fileEncrypterHelper = fileEncrypterHelper;
            _tupleProcessor = tupleProcessor;
            _configurationListHelper = configurationListHelper;
            LoginCommand = new StretchApp.Helper.RelayCommand(Login, canexecuteLogin);
            //CreateKeyCommand = new StretchApp.Helper.RelayCommand(CreateKey, canexecute);
            //ExportKeyCommand = new StretchApp.Helper.RelayCommand(ExportKey, canexecute);
            //DecryptCommand = new StretchApp.Helper.RelayCommand(Decrypt, canexecute);
            GoToCalculatorPageCommand = new Helper.RelayCommand(GoToCalculatorPage, canGoToCalculatorexecute);
         
            //if (!File.Exists(pwf))
            //{
            //    GoToIdentifierPageCommand = new Helper.RelayCommand(GoToIdentifierPage, canexecute);

            //}
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="parameter"></param>
        void GoToCalculatorPage(object parameter)
        {
            var win = new CalculatorPage { DataContext = new ViewModelCalculatorPage(_calculatorHelper, _inputProcessor, _tupleProcessor, _configurationListHelper) };
            win.Show();
            CloseWindowEvent(this, null);
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="parameter"></param>
        /// <returns></returns>
        private bool canexecute(object parameter)
        {

            return true;
        }

        private bool canexecuteLogin(object parameter)
        {
     //       if ()
            return true;
        }



        private bool canGoToCalculatorexecute(object parameter)
        {
                return identified;
        }
        private void Login(object parameter)
        {
            //  string initString = "Ali";
            PasswordBox boxPass = (PasswordBox)parameter;
            SecureString testString = new SecureString();
            using (testString = boxPass.SecurePassword)
            {
                var Password = boxPass.Password;


                if (!File.Exists(@"c:\Domino\Shrink\Key\Pwh.txt"))
                {
                    // _encrypterHelper.Decrypter("PwdToEncr.enc");
                    if (_encrypterHelper.DecryptFileCode("PwdToEncr.enc"))
                    {
                        _fileEncrypterHelper.PwdFileWithCodeEncrypter();
                        var win = new CalculatorPage { DataContext = new ViewModelCalculatorPage(_calculatorHelper, _inputProcessor, _tupleProcessor, _configurationListHelper) };

                        win.Show();
                        CloseWindowEvent(this, null);
                    }
                }
                if(!File.Exists(@"c:\Domino\Shrink\Key\PwdToEncr.enc") )

                {
                    System.Windows.MessageBox.Show("Please Copy the file PwdToEncr.enc we sent you earlier into c:\\Domino\\Shrink\\Key ");
                }

               else  if (Password == _fileEncrypterHelper.PwdFileDecrypter()) // && _encrypterHelper.Decrypter("PwdToEncr.enc")) // initString)
                {
                    identified = true;
                    testString.Dispose();
                    var win = new CalculatorPage { DataContext = new ViewModelCalculatorPage(_calculatorHelper, _inputProcessor, _tupleProcessor, _configurationListHelper) };

                    win.Show();
                    CloseWindowEvent(this, null);

                }
               else if (Password != _fileEncrypterHelper.PwdFileDecrypter())
                {
                    var str = testString.ToString();
                    System.Windows.MessageBox.Show("Password Not Correct");
                }
            }
        }
        /// <summary>
        /// 
        /// </summary>
        /// <param name="parameter"></param>
        private void CreateKey(object parameter)
        {
            _encrypterHelper.CreateKey();
            _encrypterHelper.ExportPublicKey();

        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="parameter"></param>
        private void ExportKey(object parameter)
        {

            _encrypterHelper.ExportPublicKey();

        }
        /// <summary>
        /// 
        /// </summary>
        /// <param name="parameter"></param>
        private void Decrypt(object parameter)
        {

            _encrypterHelper.Decrypter("PwdToEncr.enc");
        //    CheckCodeMatch();

        }
        /// <summary>
        /// 
        /// </summary>
        /// <param name="securePassword"></param>
        /// <returns></returns>
        private string ConvertToUnsecureString(System.Security.SecureString securePassword)
        {
            if (securePassword == null)
            {
                return string.Empty;
            }

            IntPtr unmanagedString = IntPtr.Zero;
            try
            {
                unmanagedString = System.Runtime.InteropServices.Marshal.SecureStringToGlobalAllocUnicode(securePassword);
                return System.Runtime.InteropServices.Marshal.PtrToStringUni(unmanagedString);
            }
            finally
            {
                System.Runtime.InteropServices.Marshal.ZeroFreeGlobalAllocUnicode(unmanagedString);
            }
        }

        /// <summary>
        /// 
        /// </summary>
        public void CloseApplication()
        {
            Application.Current.MainWindow.Close();
        }
        /// <summary>
        /// 
        /// </summary>
        /// <param name="propertyName"></param>
        public void OnPropertyChanged(string propertyName)
        {
            if (PropertyChanged != null)
            {
                PropertyChanged(this, new PropertyChangedEventArgs(propertyName));
            }
        }


    }
}
