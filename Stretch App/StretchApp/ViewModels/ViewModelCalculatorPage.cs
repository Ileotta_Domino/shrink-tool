﻿using StretchApp.Helper;
using StretchApp.Helper.Interfaces;
using StretchApp.Model;
using StretchApp.Parameters.Interfaces;
using StretchApp.Views;
using System;
using System.Collections.ObjectModel;
using System.ComponentModel;
using System.IO;
using System.Linq;
using System.Windows;
using System.Windows.Input;

namespace StretchApp.ViewModels
{
    class ViewModelCalculatorPage : ViewModelBase, IClosableViewModel, INotifyPropertyChanged
    {
        private readonly ICalculatorHelper _calculatorHelper;
        private readonly IinputProcessor _inputProcessor;
        public readonly ITupleProcessor _tupleProcessor;
        private readonly IConfigurationListHelper _configurationListHelper;

        public event EventHandler CloseWindowEvent;
        public event PropertyChangedEventHandler PropertyChanged;
        private int _height;

        private double _WidestCircumference;
        private double _Ind1WidestCircumference;
        private double _Ind2WidestCircumference;
        private double _Ind3WidestCircumference;
        private double _Ind4WidestCircumference;
        private double _Ind5WidestCircumference;

        private double _NarrowestCircumference;

        private double _Ind1NarrowestCircumference;
        private double _Ind2NarrowestCircumference;
        private double _Ind3NarrowestCircumference;
        private double _Ind4NarrowestCircumference;
        private double _Ind5NarrowestCircumference;

        private double _HeightOfContainer;
        private double _HeightUntilShrink;
        private double _Ind1HeightOfContainer;
        private double _Ind1HeightUntilShrink;
        private double _Ind2HeightOfContainer;
        private double _Ind2HeightUntilShrink;
        private double _Ind3HeightOfContainer;
        private double _Ind3HeightUntilShrink;
        private double _Ind4HeightOfContainer;
        private double _Ind4HeightUntilShrink;
        private double _Ind5HeightOfContainer;
        private double _Ind5HeightUntilShrink;

        private bool _Indent1IsSelected;
        private bool _Indent2IsSelected;
        private bool _Indent3IsSelected;
        private bool _Indent4IsSelected;
        private bool _Indent5IsSelected;


        string _ResultValue;
       string _Ind1ResultValue;
        string _Ind2ResultValue;
        string _Ind3ResultValue;
        string _Ind4ResultValue;
        string _Ind5ResultValue;


        string _TintValue;
        string _Ind1TintValue;
        string _Ind2TintValue;
        string _Ind3TintValue;
        string _Ind4TintValue;
        string _Ind5TintValue;

        string _StartLocation;
        string _Ind1StartLocation;
        string _Ind2StartLocation;
        string _Ind3StartLocation;
        string _Ind4StartLocation;
        string _Ind5StartLocation;
        Units _SelectedOrder;
        private string _UnitOfMeasureSelected;

        public ObservableCollection<Units> UnitsOfMeasure { get; set; }




        public string ResultValue
        {
            get
            {
                return _ResultValue;
            }
            set
            {
                if (_ResultValue != value)
                {
                    _ResultValue = value;
                    try
                    {
                        _inputProcessor.SetResultValueValue(Int32.Parse(_ResultValue));
                        OnPropertyChanged("ResultValue");
                    }
                    catch (FormatException e)
                    {
                        Console.WriteLine(e.Message);
                    }


                }
            }
        }

        public string Ind1ResultValue
        {
            get
            {
                return _Ind1ResultValue;
            }
            set
            {
                if (_Ind1ResultValue != value)
                {
                    _Ind1ResultValue = value;
                    try
                    {
                        _inputProcessor.SetResultValueValue(Int32.Parse(_Ind1ResultValue));
                        OnPropertyChanged("Ind1ResultValue");
                    }
                    catch (FormatException e)
                    {
                        Console.WriteLine(e.Message);
                    }
                }
            }
        }
        public string Ind2ResultValue
        {
            get
            {
                return _Ind2ResultValue;
            }
            set
            {
                if (_Ind2ResultValue != value)
                {
                    _Ind2ResultValue = value;
                    try
                    {
                        _inputProcessor.SetResultValueValue(Int32.Parse(_Ind2ResultValue));
                        OnPropertyChanged("Ind2ResultValue");
                    }
                    catch (FormatException e)
                    {
                        Console.WriteLine(e.Message);
                    }
                }
            }
        }
        public string Ind3ResultValue
        {
            get
            {
                return _Ind3ResultValue;
            }
            set
            {
                if (_Ind3ResultValue != value)
                {
                    _Ind3ResultValue = value;
                    try
                    {
                        _inputProcessor.SetResultValueValue(Int32.Parse(_Ind3ResultValue));
                        OnPropertyChanged("Ind3ResultValue");
                    }
                    catch (FormatException e)
                    {
                        Console.WriteLine(e.Message);
                    }
                }
            }
        }
        public string Ind4ResultValue
        {
            get
            {
                return _Ind4ResultValue;
            }
            set
            {
                if (_Ind4ResultValue != value)
                {
                    _Ind4ResultValue = value;
                    try
                    {
                        _inputProcessor.SetResultValueValue(Int32.Parse(_Ind4ResultValue));
                        OnPropertyChanged("Ind4ResultValue");
                    }
                    catch (FormatException e)
                    {
                        Console.WriteLine(e.Message);
                    }
                }
            }
        }
        public string Ind5ResultValue
        {
            get
            {
                return _Ind5ResultValue;
            }
            set
            {
                if (_Ind5ResultValue != value)
                {
                    _Ind5ResultValue = value;
                    try
                    {
                        _inputProcessor.SetResultValueValue(Int32.Parse(_Ind5ResultValue));
                        OnPropertyChanged("Ind5ResultValue");
                    }
                    catch (FormatException e)
                    {
                        Console.WriteLine(e.Message);
                    }
                }
            }
        }
        public string TintValue
        {
            get
            {
                return _TintValue;
            }
            set
            {
                if (_TintValue != value)
                {
                    _TintValue = value;
                    try
                    {
                        _inputProcessor.SetTintValueValue(Int32.Parse(_TintValue));
                        OnPropertyChanged("TintValue");
                    }
                    catch (FormatException e)
                    {
                        Console.WriteLine(e.Message);
                    }
                }
            }
        }

        public string Ind1TintValue
        {
            get
            {
                return _Ind1TintValue;
            }
            set
            {
                if (_Ind1TintValue != value)
                {
                    _Ind1TintValue = value;
                    try
                    {
                        _inputProcessor.SetInd1TintValueValue(Int32.Parse(_Ind1TintValue));
                        OnPropertyChanged("Ind1TintValue");
                    }
                    catch (FormatException e)
                    {
                        Console.WriteLine(e.Message);
                    }
                }
            }
        }

        public string Ind2TintValue
        {
            get
            {
                return _Ind2TintValue;
            }
            set
            {
                if (_Ind2TintValue != value)
                {
                    _Ind2TintValue = value;
                    try
                    {
                        _inputProcessor.SetInd2TintValueValue(Int32.Parse(_Ind2TintValue));
                        OnPropertyChanged("Ind2TintValue");
                    }
                    catch (FormatException e)
                    {
                        Console.WriteLine(e.Message);
                    }
                }
            }
        }
        public string Ind3TintValue
        {
            get
            {
                return _Ind3TintValue;
            }
            set
            {
                if (_Ind3TintValue != value)
                {
                    _Ind3TintValue = value;
                    try
                    {
                        _inputProcessor.SetInd3TintValueValue(Int32.Parse(_Ind3TintValue));
                        OnPropertyChanged("Ind3TintValue");
                    }
                    catch (FormatException e)
                    {
                        Console.WriteLine(e.Message);
                    }
                }
            }
        }
        public string Ind4TintValue
        {
            get
            {
                return _Ind4TintValue;
            }
            set
            {
                if (_Ind4TintValue != value)
                {
                    _Ind4TintValue = value;
                    try
                    {
                        _inputProcessor.SetInd4TintValueValue(Int32.Parse(_Ind4TintValue));
                        OnPropertyChanged("Ind4TintValue");
                    }
                    catch (FormatException e)
                    {
                        Console.WriteLine(e.Message);
                    }
                }
            }
        }

        public string Ind5TintValue
        {
            get
            {
                return _Ind5TintValue;
            }
            set
            {
                if (_Ind5TintValue != value)
                {
                    _Ind5TintValue = value;
                    try
                    {
                        _inputProcessor.SetInd5TintValueValue(Int32.Parse(_Ind5TintValue));
                        OnPropertyChanged("Ind5TintValue");
                    }
                    catch (FormatException e)
                    {
                        Console.WriteLine(e.Message);
                    }
                }
            }
        }

        public string StartLocation
        {
            get
            {
                return _StartLocation;
            }
            set
            {
                if (_StartLocation != value)
                {
                    _StartLocation = value;
                    try
                    {
                        _inputProcessor.SetStartLocationValue(Int32.Parse(_StartLocation));
                        OnPropertyChanged("StartLocation");
                    }
                    catch (FormatException e)
                    {
                        Console.WriteLine(e.Message);
                    }
                }
            }
        }


        public string Ind1StartLocation
        {
            get
            {
                return _Ind1StartLocation;
            }
            set
            {
                if (_Ind1StartLocation != value)
                {
                    _Ind1StartLocation = value;
                    try
                    {
                        _inputProcessor.SetInd1StartLocationValue(Int32.Parse(_Ind1StartLocation));
                        OnPropertyChanged("Ind1StartLocation");
                    }
                    catch (FormatException e)
                    {
                        Console.WriteLine(e.Message);
                    }
                }
            }
        }

        public string Ind2StartLocation
        {
            get
            {
                return _Ind2StartLocation;
            }
            set
            {
                if (_Ind2StartLocation != value)
                {
                    _Ind2StartLocation = value;
                    try
                    {
                        _inputProcessor.SetInd2StartLocationValue(Int32.Parse(_Ind2StartLocation));
                        OnPropertyChanged("Ind2StartLocation");
                    }
                    catch (FormatException e)
                    {
                        Console.WriteLine(e.Message);
                    }
                }
            }
        }

        public string Ind3StartLocation
        {
            get
            {
                return _Ind3StartLocation;
            }
            set
            {
                if (_Ind3StartLocation != value)
                {
                    _Ind3StartLocation = value;
                    try
                    {
                        _inputProcessor.SetInd3StartLocationValue(Int32.Parse(_Ind3StartLocation));
                        OnPropertyChanged("Ind3StartLocation");
                    }
                    catch (FormatException e)
                    {
                        Console.WriteLine(e.Message);
                    }
                }
            }
        }

        public string Ind4StartLocation
        {
            get
            {
                return _Ind4StartLocation;
            }
            set
            {
                if (_Ind4StartLocation != value)
                {
                    _Ind4StartLocation = value;
                    try
                    {
                        _inputProcessor.SetInd4StartLocationValue(Int32.Parse(_Ind4StartLocation));
                        OnPropertyChanged("Ind4StartLocation");
                    }
                    catch (FormatException e)
                    {
                        Console.WriteLine(e.Message);
                    }
                }
            }
        }

        public string Ind5StartLocation
        {
            get
            {
                return _Ind5StartLocation;
            }
            set
            {
                if (_Ind5StartLocation != value)
                {
                    _Ind5StartLocation = value;
                    try
                    {
                        _inputProcessor.SetInd5StartLocationValue(Int32.Parse(_Ind5StartLocation));
                        OnPropertyChanged("Ind5StartLocation");
                    }
                    catch (FormatException e)
                    {
                        Console.WriteLine(e.Message);
                    }
                }
            }
        }

        public double WidestCircumference
        {
            get
            {
                return _WidestCircumference;
            }
            set
            {
                if (_WidestCircumference != value)
                {
                    _WidestCircumference = value;
                    _inputProcessor.SetWidestCircumference(_WidestCircumference);
                    OnPropertyChanged("WidestCircumferenceToShow");
                }
            }
        }

        public double Ind1WidestCircumference
        {
            get
            {
                return _Ind1WidestCircumference;
            }
            set
            {
                if (_Ind1WidestCircumference != value)
                {
                    _Ind1WidestCircumference = value;
                    _inputProcessor.SetInd1WidestCircumference(_Ind1WidestCircumference);
                    OnPropertyChanged("Ind1WidestCircumference");
                }
            }
        } 
        public double Ind2WidestCircumference
        {
            get
            {
                return _Ind2WidestCircumference;
            }
            set
            {
                if (_Ind2WidestCircumference != value)
                {
                    _Ind2WidestCircumference = value;
                    _inputProcessor.SetInd2WidestCircumference(_Ind2WidestCircumference);
                    OnPropertyChanged("Ind2WidestCircumference");
                }
            }
        }
        public double Ind3WidestCircumference
        {
            get
            {
                return _Ind3WidestCircumference;
            }
            set
            {
                if (_Ind3WidestCircumference != value)
                {
                    _Ind3WidestCircumference = value;
                    _inputProcessor.SetInd3WidestCircumference(_Ind3WidestCircumference);
                    OnPropertyChanged("Ind3WidestCircumference");
                }
            }
        }
        public double Ind4WidestCircumference
        {
            get
            {
                return _Ind4WidestCircumference;
            }
            set
            {
                if (_Ind4WidestCircumference != value)
                {
                    _Ind4WidestCircumference = value;
                    _inputProcessor.SetInd4WidestCircumference(_Ind4WidestCircumference);
                    OnPropertyChanged("Ind4WidestCircumference");
                }
            }
        }
        public double Ind5WidestCircumference
        {
            get
            {
                return _Ind5WidestCircumference;
            }
            set
            {
                if (_Ind5WidestCircumference != value)
                {
                    _Ind5WidestCircumference = value;
                    _inputProcessor.SetInd5WidestCircumference(_Ind5WidestCircumference);
                    OnPropertyChanged("Ind5WidestCircumference");
                }
            }
        }

        public double NarrowestCircumference
        {
            get
            {
                return _NarrowestCircumference;
            }
            set
            {
                if (_NarrowestCircumference != value)
                {
                    _NarrowestCircumference = value;
                    _inputProcessor.SetNarrowestCircumferenceValue(_NarrowestCircumference);
                    OnPropertyChanged("NarrowestCircumference");
                }
            }
        }

        public double Ind1NarrowestCircumference
        {
            get
            {
                return _Ind1NarrowestCircumference;
            }
            set
            {
                if (_Ind1NarrowestCircumference != value)
                {
                    _Ind1NarrowestCircumference = value;
                    _inputProcessor.SetInd1NarrowestCircumferenceValue(_Ind1NarrowestCircumference);
                    OnPropertyChanged("Ind1NarrowestCircumference");
                }
            }
        }

        public double Ind2NarrowestCircumference
        {
            get
            {
                return _Ind2NarrowestCircumference;
            }
            set
            {
                if (_Ind2NarrowestCircumference != value)
                {
                    _Ind2NarrowestCircumference = value;
                    _inputProcessor.SetInd2NarrowestCircumferenceValue(_Ind2NarrowestCircumference);
                    OnPropertyChanged("Ind2NarrowestCircumference");
                }
            }
        }
        public double Ind3NarrowestCircumference
        {
            get
            {
                return _Ind3NarrowestCircumference;
            }
            set
            {
                if (_Ind3NarrowestCircumference != value)
                {
                    _Ind3NarrowestCircumference = value;
                    _inputProcessor.SetInd3NarrowestCircumferenceValue(_Ind3NarrowestCircumference);
                    OnPropertyChanged("Ind3NarrowestCircumference");
                }
            }
        }
        public double Ind4NarrowestCircumference
        {
            get
            {
                return _Ind4NarrowestCircumference;
            }
            set
            {
                if (_Ind4NarrowestCircumference != value)
                {
                    _Ind4NarrowestCircumference = value;
                    _inputProcessor.SetInd4NarrowestCircumferenceValue(_Ind4NarrowestCircumference);
                    OnPropertyChanged("Ind4NarrowestCircumference");
                }
            }
        }
        public double Ind5NarrowestCircumference
        {
            get
            {
                return _Ind5NarrowestCircumference;
            }
            set
            {
                if (_Ind5NarrowestCircumference != value)
                {
                    _Ind5NarrowestCircumference = value;
                    _inputProcessor.SetInd5NarrowestCircumferenceValue(_Ind5NarrowestCircumference);
                    OnPropertyChanged("Ind5NarrowestCircumference");
                }
            }
        }


        public double HeightOfContainer
        {
            get
            {
                return _HeightOfContainer;
            }
            set
            {
                if (_HeightOfContainer != value)
                {
                    _HeightOfContainer = value;
                    _inputProcessor.SetHeightOfContainerValue(_HeightOfContainer);
                    OnPropertyChanged("HeightOfContainer");
                }
            }
        }

        public double Ind1HeightOfContainer
        {
            get
            {
                return _Ind1HeightOfContainer;
            }
            set
            {
                if (_Ind1HeightOfContainer != value)
                {
                    _Ind1HeightOfContainer = value;
                    _inputProcessor.SetHeightOfContainerValue(_Ind1HeightOfContainer);
                    OnPropertyChanged("Ind1HeightOfContainer");
                }
            }
        }

        public double Ind1HeightUntilShrink
        {
            get
            {
                return _Ind1HeightUntilShrink;
            }
            set
            {
                if (_Ind1HeightUntilShrink != value)
                {
                    _Ind1HeightUntilShrink = value;
                    _inputProcessor.SetHeightUntilShrinkInd1Value(_Ind1HeightUntilShrink);
                    OnPropertyChanged("Ind1HeightUntilShrink");
                }
            }
        }

        public double Ind2HeightOfContainer
        {
            get
            {
                return _Ind2HeightOfContainer;
            }
            set
            {
                if (_Ind2HeightOfContainer != value)
                {
                    _Ind2HeightOfContainer = value;
                    _inputProcessor.SetHeightOfContainerInd2Value(_Ind2HeightOfContainer);
                    OnPropertyChanged("Ind2HeightOfContainer");
                }
            }
        }

        public double Ind2HeightUntilShrink
        {
            get
            {
                return _Ind2HeightUntilShrink;
            }
            set
            {
                if (_Ind2HeightUntilShrink != value)
                {
                    _Ind2HeightUntilShrink = value;
                    _inputProcessor.SetHeightUntilShrinkInd2Value(_Ind2HeightUntilShrink);
                    OnPropertyChanged("Ind2HeightUntilShrink");
                }
            }
        }
        public double Ind3HeightOfContainer
        {
            get
            {
                return _Ind3HeightOfContainer;
            }
            set
            {
                if (_Ind3HeightOfContainer != value)
                {
                    _Ind3HeightOfContainer = value;
                    _inputProcessor.SetHeightOfContainerValue(_Ind3HeightOfContainer);
                    OnPropertyChanged("Ind3HeightOfContainer");
                }
            }
        }

        public double Ind3HeightUntilShrink
        {
            get
            {
                return _Ind3HeightUntilShrink;
            }
            set
            {
                if (_Ind3HeightUntilShrink != value)
                {
                    _Ind3HeightUntilShrink = value;
                    _inputProcessor.SetHeightUntilShrinkInd3Value(_Ind3HeightUntilShrink);
                    OnPropertyChanged("Ind3HeightUntilShrink");
                }
            }
        }
        public double Ind4HeightOfContainer
        {
            get
            {
                return _Ind4HeightOfContainer;
            }
            set
            {
                if (_Ind4HeightOfContainer != value)
                {
                    _Ind4HeightOfContainer = value;
                    _inputProcessor.SetHeightOfContainerValue(_Ind4HeightOfContainer);
                    OnPropertyChanged("Ind4HeightOfContainer");
                }
            }
        }

        public double Ind4HeightUntilShrink
        {
            get
            {
                return _Ind4HeightUntilShrink;
            }
            set
            {
                if (_Ind4HeightUntilShrink != value)
                {
                    _Ind4HeightUntilShrink = value;
                    _inputProcessor.SetHeightUntilShrinkInd4Value(_Ind4HeightUntilShrink);
                    OnPropertyChanged("Ind4HeightUntilShrink");
                }
            }
        }

        public double Ind5HeightOfContainer
        {
            get
            {
                return _Ind5HeightOfContainer;
            }
            set
            {
                if (_Ind5HeightOfContainer != value)
                {
                    _Ind5HeightOfContainer = value;
                    _inputProcessor.SetHeightOfContainerValue(_Ind5HeightOfContainer);
                    OnPropertyChanged("Ind5HeightOfContainer");
                }
            }
        }

        public double Ind5HeightUntilShrink
        {
            get
            {
                return _Ind5HeightUntilShrink;
            }
            set
            {
                if (_Ind5HeightUntilShrink != value)
                {
                    _Ind5HeightUntilShrink = value;
                    _inputProcessor.SetHeightUntilShrinkInd5Value(_Ind5HeightUntilShrink);
                    OnPropertyChanged("Ind5HeightUntilShrink");
                }
            }
        }

        public double HeightUntilShrink
        {
            get
            {
                return _HeightUntilShrink;
            }
            set
            {
                if (_HeightUntilShrink != value)
                {
                    _HeightUntilShrink = value;
                    _inputProcessor.SetHeightUntilShrinkValue(_HeightUntilShrink);
                    OnPropertyChanged("HeightUntilShrink");
                }
            }
        }

        public Units SelectedOrder
        {
            get
            {
             
                return _SelectedOrder;
            }
            set
            {
                if (_SelectedOrder != value)
                {
                    _SelectedOrder = value;
                    _inputProcessor.SetUnits(_SelectedOrder.UnitsName.ToString());
                    UnitOfMeasureSelected = _inputProcessor.GetUnits();
                    OnPropertyChanged("SelectedOrder");
                }
            }
        }

        public string UnitOfMeasureSelected
        {
            get
            {
                return _UnitOfMeasureSelected;
            }
            set
            {
                if (_UnitOfMeasureSelected != value)
                {
                    _UnitOfMeasureSelected = value;
                    if(_UnitOfMeasureSelected=="Inches")
                        _UnitOfMeasureSelected="''";
                  //  _inputProcessor.SetUnits(_UnitOfMeasureSelected);
                    OnPropertyChanged("UnitOfMeasureSelected");
                }
            }
        }

        public int CustomHeight
        {
            get { return _height; }
            set
            {
                if (value != _height)
                {
                    _height = value;
                    if (PropertyChanged != null)
                        PropertyChanged(this, new PropertyChangedEventArgs("CustomHeight"));
                }
            }
        }

        public bool Indent1IsSelected
        {
            get { return _Indent1IsSelected; }
            set
            {
                if (_Indent1IsSelected == value) return;


                _Indent1IsSelected = value;
                if (value) { CustomHeight = 510; }
                else { CustomHeight = 468;
                    Indent2IsSelected = false;
                    Indent3IsSelected = false;
                    Indent4IsSelected = false;
                    Indent5IsSelected = false;
                }

                _inputProcessor.SetIndent1IsSelectedValue(_Indent1IsSelected);
                OnPropertyChanged("Indent1IsSelected");
            }
        }

        public bool Indent2IsSelected
        {
            get { return _Indent2IsSelected; }
            set
            {
                if (_Indent2IsSelected == value) return;

                //_inputProcessor.SetIndent1IsSelectedValue(_Indent2IsSelected);
                _Indent2IsSelected = value;
                //     Indent1IsSelected = !_inputProcessor.GetIndent1IsSelectedValue();
                CustomHeight = 468;
                if (value) { 
                    Indent1IsSelected = _Indent2IsSelected;
                CustomHeight = 570;
            }
                                else {
                    ////Indent1IsSelected = !_Indent2IsSelected;
                    Indent3IsSelected = false;
                    Indent4IsSelected = false;
                    Indent5IsSelected = false;
                }
            _inputProcessor.SetIndent2IsSelectedValue(_Indent2IsSelected);
                OnPropertyChanged("Indent2IsSelected");
            }
        }

        public bool Indent3IsSelected
        {
            get { return _Indent3IsSelected; }
            set
            {
                if (_Indent3IsSelected == value) return;
                CustomHeight = 570;
                _Indent3IsSelected = value;

                //   Indent2IsSelected = !_inputProcessor.GetIndent2IsSelectedValue();
                if (value) { 
                    Indent2IsSelected = _Indent3IsSelected;
                CustomHeight = 655; }
                                else {
                    Indent4IsSelected = false;
                    Indent5IsSelected = false;
                    //Int2IsSelected = !_Indent3IsSelected;
                    //Indent4IsSelected = _Indent3IsSelected;
                    //Indent5IsSelected = _Indent4IsSelected;
                }
                _inputProcessor.SetIndent2IsSelectedValue(_Indent2IsSelected);
                OnPropertyChanged("Indent3IsSelected");
            }
        }

        public bool Indent4IsSelected
        {
            get { return _Indent4IsSelected; }
            set
            {
                if (_Indent4IsSelected == value) return;
                CustomHeight = 655;
                _Indent4IsSelected = value;
                //  Indent3IsSelected = !_inputProcessor.GetIndent3IsSelectedValue();
                if (value)
                {
                    Indent3IsSelected = true;

                    CustomHeight = 745;
                }
                else { //Indent3IsSelected = !_Indent4IsSelected;
                    Indent5IsSelected = false;
                }
                _inputProcessor.SetIndent4IsSelectedValue(_Indent4IsSelected);
                OnPropertyChanged("Indent4IsSelected");
            }
        }

        public bool Indent5IsSelected
        {
            get { return _Indent5IsSelected; }
            set
            {
                if (_Indent5IsSelected == value) return;
                CustomHeight = 745;
                _Indent5IsSelected = value;

                //   Indent4IsSelected = !_inputProcessor.GetIndent4IsSelectedValue();
                if (value)
                {
                    Indent4IsSelected = _Indent5IsSelected;
                    CustomHeight = 840;
                }
                //else
                //{ Indent4IsSelected = false; }
                    //    //Indent4IsSelected = !_Indent5IsSelected;
                    //    Indent4IsSelected = false;
                    //  }
                    _inputProcessor.SetIndent5IsSelectedValue(_Indent5IsSelected);
                OnPropertyChanged("Indent5IsSelected");
            }
        }

        public RelayCommand CalculateCommand { get; set; }
        public RelayCommand GoOpenConfigurationPageCommand { get; set; }
        public RelayCommand GoOpenAboutPageCommand { get; set; }

        public ViewModelCalculatorPage(ICalculatorHelper calculatorHelper,
            IinputProcessor inputProcessor, ITupleProcessor tupleProcessor, IConfigurationListHelper configurationListHelper)
        {
            _calculatorHelper = calculatorHelper;
            _inputProcessor = inputProcessor;
            _tupleProcessor = tupleProcessor;
            _configurationListHelper = configurationListHelper;

            CustomHeight = 468;
            UnitsOfMeasure = new ObservableCollection<Units>
             {
                 new Units( _inputProcessor){ UnitsName="mm"},
                 new Units(_inputProcessor){ UnitsName="Inches"}

            };
            SelectedOrder = UnitsOfMeasure.First();
            //     UnitOfMeasureSelected = UnitsOfMeasureFirst().ToString() ;
            if (_inputProcessor.GetWidestCircumference() != 1)
            { WidestCircumference = _inputProcessor.GetWidestCircumference(); }
            else { _inputProcessor.SetWidestCircumference(1.00); }

            if (_inputProcessor.GetNarrowestCircumferenceValue() != 1)
            { NarrowestCircumference = _inputProcessor.GetNarrowestCircumferenceValue(); }
            else { _inputProcessor.SetNarrowestCircumferenceValue(1.00); }

            if (_inputProcessor.GetHeightOfContainerValue() != 1)
            { HeightOfContainer = _inputProcessor.GetHeightOfContainerValue(); }
            else { _inputProcessor.SetHeightOfContainerValue(1.00); }

            if (_inputProcessor.GetHeightOfContainerInd1Value() != 1)
            { Ind1HeightOfContainer = _inputProcessor.GetHeightOfContainerInd1Value(); }
            else { _inputProcessor.SetHeightOfContainerInd1Value(1.00); }

            if (_inputProcessor.GetHeightUntilShrinkInd1Value() != 1)
            { Ind1HeightUntilShrink = _inputProcessor.GetHeightUntilShrinkInd1Value(); }
            else { _inputProcessor.SetHeightUntilShrinkInd1Value(1.00); }

            if (_inputProcessor.GetHeightOfContainerInd2Value() != 1)
            { Ind2HeightOfContainer = _inputProcessor.GetHeightOfContainerInd2Value(); }
            else { _inputProcessor.SetHeightOfContainerInd2Value(1.00); }

            if (_inputProcessor.GetHeightUntilShrinkInd2Value() != 1)
            { Ind2HeightUntilShrink = _inputProcessor.GetHeightUntilShrinkInd2Value(); }
            else { _inputProcessor.SetHeightUntilShrinkInd2Value(1.00); }

            if (_inputProcessor.GetHeightOfContainerInd3Value() != 1)
            { Ind3HeightOfContainer = _inputProcessor.GetHeightOfContainerInd3Value(); }
            else { _inputProcessor.SetHeightOfContainerInd3Value(1.00); }

            if (_inputProcessor.GetHeightUntilShrinkInd3Value() != 1)
            { Ind3HeightUntilShrink = _inputProcessor.GetHeightUntilShrinkInd3Value(); }
            else { _inputProcessor.SetHeightUntilShrinkInd3Value(1.00); }

            if (_inputProcessor.GetHeightOfContainerInd4Value() != 1)
            { Ind4HeightOfContainer = _inputProcessor.GetHeightOfContainerInd4Value(); }
            else { _inputProcessor.SetHeightOfContainerInd4Value(1.00); }

            if (_inputProcessor.GetHeightUntilShrinkInd4Value() != 1)
            { Ind4HeightUntilShrink = _inputProcessor.GetHeightUntilShrinkInd4Value(); }
            else { _inputProcessor.SetHeightUntilShrinkInd4Value(1.00); }


            if (_inputProcessor.GetHeightOfContainerInd5Value() != 1)
            { Ind5HeightOfContainer = _inputProcessor.GetHeightOfContainerInd5Value(); }
            else { _inputProcessor.SetHeightOfContainerInd5Value(1.00); }

            if (_inputProcessor.GetHeightUntilShrinkInd5Value() != 1)
            { Ind5HeightUntilShrink = _inputProcessor.GetHeightUntilShrinkInd5Value(); }
            else { _inputProcessor.SetHeightUntilShrinkInd5Value(1.00); }

            if (_inputProcessor.GetHeightUntilShrinkValue() != 1)
            { HeightUntilShrink = _inputProcessor.GetHeightUntilShrinkValue(); }
            else { _inputProcessor.SetHeightUntilShrinkValue(1.00); }


            if (_inputProcessor.GetIndent1IsSelectedValue() != false)
            { Indent1IsSelected = _inputProcessor.GetIndent1IsSelectedValue(); }
            else { _inputProcessor.SetIndent1IsSelectedValue(false); }

            if (_inputProcessor.GetIndent2IsSelectedValue() != false)
            { Indent2IsSelected = _inputProcessor.GetIndent2IsSelectedValue(); }
            else { _inputProcessor.SetIndent2IsSelectedValue(false); }

            if (_inputProcessor.GetIndent3IsSelectedValue() != false)
            { Indent3IsSelected = _inputProcessor.GetIndent3IsSelectedValue(); }
            else { _inputProcessor.SetIndent3IsSelectedValue(false); }

            if (_inputProcessor.GetIndent4IsSelectedValue() != false)
            { Indent4IsSelected = _inputProcessor.GetIndent4IsSelectedValue(); }
            else { _inputProcessor.SetIndent4IsSelectedValue(false); }

            if (_inputProcessor.GetIndent5IsSelectedValue() != false)
            { Indent5IsSelected = _inputProcessor.GetIndent5IsSelectedValue(); }
            else { _inputProcessor.SetIndent5IsSelectedValue(false); }


            //_inputProcessor.SetWidestCircumference(1);
            //_inputProcessor.SetNarrowestCircumferenceValue(1);
            //_inputProcessor.SetHeightOfContainerValue(1);
            //_inputProcessor.SetHeightUntilShrinkValue(1);
            WidestCircumference = _inputProcessor.GetWidestCircumference();
            NarrowestCircumference = _inputProcessor.GetNarrowestCircumferenceValue();
            HeightOfContainer = _inputProcessor.GetHeightOfContainerValue();
            HeightUntilShrink = _inputProcessor.GetHeightUntilShrinkValue();

            Ind1HeightOfContainer = _inputProcessor.GetHeightOfContainerInd1Value();
            Ind1HeightUntilShrink = _inputProcessor.GetHeightUntilShrinkInd1Value();
            Ind2HeightOfContainer = _inputProcessor.GetHeightOfContainerInd2Value();
            Ind2HeightUntilShrink = _inputProcessor.GetHeightUntilShrinkInd2Value();
            Ind3HeightOfContainer = _inputProcessor.GetHeightOfContainerInd3Value();
            Ind3HeightUntilShrink = _inputProcessor.GetHeightUntilShrinkInd3Value();
            Ind4HeightOfContainer = _inputProcessor.GetHeightOfContainerInd4Value();
            Ind4HeightUntilShrink = _inputProcessor.GetHeightUntilShrinkInd4Value();
            Ind5HeightOfContainer = _inputProcessor.GetHeightOfContainerInd5Value();
            Ind5HeightUntilShrink = _inputProcessor.GetHeightUntilShrinkInd5Value();

            CalculateCommand = new StretchApp.Helper.RelayCommand(Calculate, canexecute);
            GoOpenConfigurationPageCommand = new Helper.RelayCommand(GoToConfigurationPage, canexecute);
            GoOpenAboutPageCommand = new Helper.RelayCommand(GoToAboutPage, canexecute);
        }

        private bool canexecute(object parameter)
        {

            bool res = false;
            if (File.Exists(@"c:\Domino\Shrink\Key\Pwh.txt"))
            {
                res = true;
            }
            return res;
        }

        public void Calculate(object parameter)
        {
            int temptint = 0;
            if (parameter == null) return;
            var values = (object[])parameter;
            _inputProcessor.SetWidestCircumference(WidestCircumference);
            _inputProcessor.SetNarrowestCircumferenceValue(NarrowestCircumference);

            _inputProcessor.SetResultValueValue(_calculatorHelper.CalulculatePercentage(_inputProcessor.GetWidestCircumference(), _inputProcessor.GetNarrowestCircumferenceValue()));
            _inputProcessor.SetHeightOfContainerValue(HeightOfContainer);
            _inputProcessor.SetHeightUntilShrinkValue(HeightUntilShrink);

            _inputProcessor.SetHeightOfContainerInd1Value(Ind1HeightOfContainer);
            

            _inputProcessor.SetStartLocationValue(_calculatorHelper.CalulculateStartLocationPercentage(_inputProcessor.GetHeightOfContainerValue(), _inputProcessor.GetHeightUntilShrinkValue()));


            ResultValue = _inputProcessor.GetResultValueValue().ToString();
            StartLocation = _inputProcessor.GetStartLocationValue().ToString();


            _calculatorHelper.GetTint(_inputProcessor.GetResultValueValue());
            temptint = _calculatorHelper.GetTint(_inputProcessor.GetResultValueValue());
            TintValue = _calculatorHelper.GetTintToShow(temptint);


            if (Indent1IsSelected)
            {
                _inputProcessor.SetInd1StartLocationValue(_calculatorHelper.CalulculateStartLocationPercentage(_inputProcessor.GetHeightOfContainerValue(), _inputProcessor.GetHeightUntilShrinkInd1Value()));
                Ind1StartLocation = _inputProcessor.GetInd1StartLocationValue().ToString();

                _inputProcessor.SetInd1ResultValueValue(_calculatorHelper.CalulculatePercentage(_inputProcessor.GetInd1WidestCircumference(), _inputProcessor.GetInd1NarrowestCircumferenceValue()));
                temptint = _calculatorHelper.GetTint(_inputProcessor.GetInd1ResultValueValue());
                Ind1TintValue = _calculatorHelper.GetTintToShow(temptint);
            }
            if (Indent2IsSelected)
            {
                _inputProcessor.SetInd2StartLocationValue(_calculatorHelper.CalulculateStartLocationPercentage(_inputProcessor.GetHeightOfContainerValue(), _inputProcessor.GetHeightUntilShrinkInd2Value()));
                Ind2StartLocation = _inputProcessor.GetInd2StartLocationValue().ToString();

                _inputProcessor.SetInd2ResultValueValue(_calculatorHelper.CalulculatePercentage(_inputProcessor.GetInd2WidestCircumference(), _inputProcessor.GetInd2NarrowestCircumferenceValue()));
                temptint = _calculatorHelper.GetTint(_inputProcessor.GetInd2ResultValueValue());
                Ind2TintValue = _calculatorHelper.GetTintToShow(temptint);
            }
            if (Indent3IsSelected)
            {
                _inputProcessor.SetInd3StartLocationValue(_calculatorHelper.CalulculateStartLocationPercentage(_inputProcessor.GetHeightOfContainerValue(), _inputProcessor.GetHeightUntilShrinkInd3Value()));
                Ind3StartLocation = _inputProcessor.GetInd3StartLocationValue().ToString();

                _inputProcessor.SetInd3ResultValueValue(_calculatorHelper.CalulculatePercentage(_inputProcessor.GetInd3WidestCircumference(), _inputProcessor.GetInd3NarrowestCircumferenceValue()));
                temptint = _calculatorHelper.GetTint(_inputProcessor.GetInd3ResultValueValue());
                Ind3TintValue = _calculatorHelper.GetTintToShow(temptint);
            }
            if (Indent4IsSelected)
            {
                _inputProcessor.SetInd4StartLocationValue(_calculatorHelper.CalulculateStartLocationPercentage(_inputProcessor.GetHeightOfContainerValue(), _inputProcessor.GetHeightUntilShrinkInd4Value()));
                Ind4StartLocation = _inputProcessor.GetInd4StartLocationValue().ToString();

                _inputProcessor.SetInd4ResultValueValue(_calculatorHelper.CalulculatePercentage(_inputProcessor.GetInd4WidestCircumference(), _inputProcessor.GetInd4NarrowestCircumferenceValue()));
                temptint = _calculatorHelper.GetTint(_inputProcessor.GetInd4ResultValueValue());
                Ind4TintValue = _calculatorHelper.GetTintToShow(temptint);
            }
            if (Indent5IsSelected)
            {
                _inputProcessor.SetInd5StartLocationValue(_calculatorHelper.CalulculateStartLocationPercentage(_inputProcessor.GetHeightOfContainerValue(), _inputProcessor.GetHeightUntilShrinkInd5Value()));
                Ind5StartLocation = _inputProcessor.GetInd5StartLocationValue().ToString();

                _inputProcessor.SetInd5ResultValueValue(_calculatorHelper.CalulculatePercentage(_inputProcessor.GetInd5WidestCircumference(), _inputProcessor.GetInd5NarrowestCircumferenceValue()));
                temptint = _calculatorHelper.GetTint(_inputProcessor.GetInd5ResultValueValue());
                Ind5TintValue = _calculatorHelper.GetTintToShow(temptint);
            }

        }

        void GoToConfigurationPage(object parameter)
        {
            var win = new ConfigurationPage { DataContext = new ViewModelConfigurationPage( _inputProcessor, _calculatorHelper, _tupleProcessor, _configurationListHelper) };
            win.ShowDialog();
          //  CloseWindowEvent(this, null);
        }

        void GoToAboutPage(object parameter)
        {
            var win = new AboutPage { DataContext = new ViewModelAboutPage(_inputProcessor, _calculatorHelper, _tupleProcessor, _configurationListHelper) };
            win.ShowDialog();
        }
        
        public void OnPropertyChanged(string propertyName)
        {
            if (PropertyChanged != null)
            {
                PropertyChanged(this, new PropertyChangedEventArgs(propertyName));
            }
        }
    }
}
