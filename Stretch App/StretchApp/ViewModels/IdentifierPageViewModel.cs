﻿using StretchApp.Helper;
using StretchApp.Helper.Interfaces;
using StretchApp.Parameters.Interfaces;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.IO;
using System.Linq;
using System.Security;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Controls;
using System.Windows.Input;

namespace StretchApp.ViewModels
{
    class IdentifierPageViewModel : ViewModelBase, IClosableViewModel
    {
        private string sernumber;
        private readonly ISerialNumberHelper _serialNumberHelper;
        private readonly IinputProcessor _inputProcessor;
        private readonly IFileEncrypterHelper _fileEncrypterHelper;
        private readonly IEncrypterHelper _encrypterHelper;
        private readonly ICalculatorHelper _calculatorHelper;
        private readonly ITupleProcessor _tupleProcessor;
        private readonly IConfigurationListHelper _configurationListHelper;
        private string _Name;

        public event EventHandler CloseWindowEvent;
        public event PropertyChangedEventHandler PropertyChanged;
        private string Pwd { get; set; }



        public Name _NameToShow { get; set; }
        public string Name
        {
            get
            {
                return _Name;
            }
            set
            {
                if (_Name != value)
                {
                    _Name = value;
                    _inputProcessor.SetName(_Name);
                    OnPropertyChanged("Name");
                }
            }
        }

        public RelayCommand SendPasswordCommand { get; set; }
        public RelayCommand ConfirmPasswordCommand { get; set; }
        //     public RelayCommand FirstPasswordOkCommand { get; set; }
        public IdentifierPageViewModel(ISerialNumberHelper serialNumberHelper, IinputProcessor inputProcessor, IFileEncrypterHelper fileEncrypterHelper, IEncrypterHelper encrypterHelper, ICalculatorHelper calculatorHelper, ITupleProcessor tupleProcessor, IConfigurationListHelper configurationListHelper)
        {


            _serialNumberHelper = serialNumberHelper;
            _inputProcessor =inputProcessor;
            _fileEncrypterHelper = fileEncrypterHelper;
            _encrypterHelper = encrypterHelper;
            _calculatorHelper = calculatorHelper;
            _tupleProcessor = tupleProcessor;
            _configurationListHelper = configurationListHelper;
            _NameToShow = new Name(_inputProcessor);

           
            //   Name = _Name;
            //   SendPasswordCommand = new StretchApp.Helper.RelayCommand(SendPassword, canexecute);
            //   FirstPasswordOkCommand = new StretchApp.Helper.RelayCommand(FirstPasswordOkPassword, canexecute);
            SendPasswordCommand = new RelayCommand(SendPassword, canexecute);
            ConfirmPasswordCommand = new RelayCommand(ConfirmPassword, canexecute);
        }

        private bool canexecute(object parameter)
        {
            bool res = false;
            //if (File.Exists(@"c:\encrypt\hlp"))
            //{ res = true; }
            return res=_inputProcessor.GetEnabledFlag();
        }

        public void ConfirmPassword(object parameter)
        {
            var values = (object[])parameter;
            if(values[0]==values[1])
                System.Windows.MessageBox.Show("PWDs ok");
         //   PasswordBox PwdPass = (PasswordBox)parameter;

        }


            public void SendPassword(object parameter)
        {
            _encrypterHelper.AddCode();
            sernumber =_serialNumberHelper.TakeSerialNumber();
          //  File.AppendAllText(@"c:\encrypt\sernumb.txt", sernumber);
            PasswordBox boxPass = (PasswordBox)parameter;
            SecureString testString = new SecureString();
            _fileEncrypterHelper.PwdFileEncrypter();
        //    _NameToShow.NameToShow = _NameToShow.NameToShow;
            _inputProcessor.SetName(Name);
            _inputProcessor.SetReleasedToValueValue(Name);
            //   _NameToShow.NameToShow = _inputProcessor.GetName();
            Directory.CreateDirectory(@"c:\Domino\Shrink\FromUser");
            using (StreamWriter twBis = File.CreateText(@"c:\Domino\Shrink\FromUser\sernumb.txt"))
            {
                string line= _inputProcessor.GetName();
                twBis.WriteLine(_inputProcessor.GetName());
                twBis.WriteLine(_serialNumberHelper.TakeSerialNumber());
            }

            _encrypterHelper.CreateKey();
            _encrypterHelper.ExportPublicKey();

            System.Windows.MessageBox.Show("Please send the file  c:\\FromUser\\sernumb.txt to keyRequests@DominoSupport.help");
            var win = new MainWindow { DataContext = new MainViewModel(_encrypterHelper, _inputProcessor, _calculatorHelper, _serialNumberHelper, _fileEncrypterHelper, _tupleProcessor, _configurationListHelper) };
            win.Show();
            CloseWindowEvent(this, null);
            CloseWindow();
        }


        public void OnPropertyChanged(string propertyName)
        {
            if (PropertyChanged != null)
            {
                PropertyChanged(this, new PropertyChangedEventArgs(propertyName));
            }
        }
    }
}
