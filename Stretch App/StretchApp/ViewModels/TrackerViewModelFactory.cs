﻿//using StretchApp.Helper.Interfaces;
//using System;
//using System.Collections.Generic;
//using System.Linq;
//using System.Text;
//using System.Threading.Tasks;

//namespace StretchApp.ViewModels
//{
//    internal interface ITrackerViewModelFactory
//    {
//        TrackerViewModel Create(IWindow window);
//    }

//    internal class TrackerViewModelFactory : ITrackerViewModelFactory
//    {
//        private readonly IEncrypterHelper _encrypterHelper;

//        public TrackerViewModelFactory(IEncrypterHelper encrypterHelper)
//        {
//            if (encrypterHelper == null)
//            {
//                throw new ArgumentNullException("encrypterHelper");
//            }

//            _encrypterHelper = encrypterHelper;
//        }

//        public TrackerViewModel Create(IWindow window)
//        {
//            if (window == null)
//            {
//                throw new ArgumentNullException("window");
//            }

//            return new TrackerViewModel(_encrypterHelper, window);
//        }
//    }
//}
