﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace StretchApp.Parameters
{
    public class InputKeeper
    {
        private static int Input1=1;
        private static int Input2 = 1;
        public static string ID = "";
        public static string ReleasedTo = "";
        private static double WidestCircumference = 1.00;
        private static double Ind1WidestCircumference = 1.00;
        private static double Ind2WidestCircumference = 1.00;
        private static double Ind3WidestCircumference = 1.00;
        private static double Ind4WidestCircumference = 1.00;
        private static double Ind5WidestCircumference = 1.00;
        private static double NarrowestCircumference =1.00;

        private static double Ind1NarrowestCircumference = 1.00;
        private static double Ind2NarrowestCircumference = 1.00;
        private static double Ind3NarrowestCircumference = 1.00;
        private static double Ind4NarrowestCircumference = 1.00;
        private static double Ind5NarrowestCircumference = 1.00;

        private static double HeightOfContainer = 1.00;
        private static double HeightUntilShrink = 1.00;
        private static double HeightOfContainerInd1 = 1.00;
        private static double HeightUntilShrinkInd1=1.00;
        private static double Ind2HeightOfContainer = 1.00;
        private static double Ind2HeightUntilShrink = 1.00;
        private static double Ind3HeightOfContainer = 1.00;
        private static double Ind3HeightUntilShrink = 1.00;
        private static double Ind4HeightOfContainer = 1.00;
        private static double Ind4HeightUntilShrink = 1.00;
        private static double Ind5HeightOfContainer = 1.00;
        private static double Ind5HeightUntilShrink = 1.00;

        private static bool Indent1IsSelected = false;
        private static bool Indent2IsSelected = false;
        private static bool Indent3IsSelected = false;
        private static bool Indent4IsSelected = false;
        private static bool Indent5IsSelected = false;

        private static string Name = "John Smith";
        private static bool EnabledFlag = false;
        private static int ResultValue = 0;
        private static int Ind1ResultValue = 0;
        private static int Ind2ResultValue = 0;
        private static int Ind3ResultValue = 0;
        private static int Ind4ResultValue = 0;
        private static int Ind5ResultValue = 0;
        private static int TintValue = 0;

        private static int Ind1TintValue = 0;
        private static int Ind2TintValue = 0;
        private static int Ind3TintValue = 0;
        private static int Ind4TintValue = 0;
        private static int Ind5TintValue = 0;



        private static int StartLocation = 0;
        private static int Ind1StartLocation = 0;
        private static int Ind2StartLocation = 0;
        private static int Ind3StartLocation = 0;
        private static int Ind4StartLocation = 0;
        private static int Ind5StartLocation = 0;
        private static string UnitsSelected = "mm";
        private static string UnitOfMeasureSelected;
        private static string ConfigurationFile = "";
        public static int InputValue1
        {
            get { return Input1; }
            set { Input1 = value; }
        }

        public static int InputValue2
        {
            get { return Input2; }
            set { Input2 = value; }
        }
        public static string IDValue
        {
            get { return ID; }
            set { ID = value; }
        }
        public static double WidestCircumferenceValue
        {
            get { return WidestCircumference; }
            set { WidestCircumference = value; }
        }


        public static double Ind1WidestCircumferenceValue
        {
            get { return Ind1WidestCircumference; }
            set { Ind1WidestCircumference = value; }
        }

        public static double Ind2WidestCircumferenceValue
        {
            get { return Ind2WidestCircumference; }
            set { Ind2WidestCircumference = value; }
        }
        public static double Ind3WidestCircumferenceValue
        {
            get { return Ind3WidestCircumference; }
            set { Ind3WidestCircumference = value; }
        }
        public static double Ind4WidestCircumferenceValue
        {
            get { return Ind4WidestCircumference; }
            set { Ind4WidestCircumference = value; }
        }
        public static double Ind5WidestCircumferenceValue
        {
            get { return Ind5WidestCircumference; }
            set { Ind5WidestCircumference = value; }
        }
        
        public static double NarrowestCircumferenceValue
        {
            get { return NarrowestCircumference; }
            set { NarrowestCircumference = value; }
        }


        public static double Ind1NarrowestCircumferenceValue
        {
            get { return Ind1NarrowestCircumference; }
            set { Ind1NarrowestCircumference = value; }
        }
        public static double Ind2NarrowestCircumferenceValue
        {
            get { return Ind2NarrowestCircumference; }
            set { Ind2NarrowestCircumference = value; }
        }
        public static double Ind3NarrowestCircumferenceValue
        {
            get { return Ind3NarrowestCircumference; }
            set { Ind3NarrowestCircumference = value; }
        }
        public static double Ind4NarrowestCircumferenceValue
        {
            get { return Ind4NarrowestCircumference; }
            set { Ind4NarrowestCircumference = value; }
        }
        public static double Ind5NarrowestCircumferenceValue
        {
            get { return Ind5NarrowestCircumference; }
            set { Ind5NarrowestCircumference = value; }
        }

        public static double HeightOfContainerValue
        {
            get { return HeightOfContainer; }
            set { HeightOfContainer = value; }
        }
        public static double HeightOfContainerInd1Value
        {
            get { return HeightOfContainerInd1; }
            set { HeightOfContainerInd1 = value; }
        }

        public static double HeightUntilShrinkInd1Value
        {
            get { return HeightUntilShrinkInd1; }
            set { HeightUntilShrinkInd1 = value; }
        }
        
        public static double Ind2HeightOfContainerValue
        {
            get { return Ind2HeightOfContainer; }
            set { Ind2HeightOfContainer = value; }
        }
        public static double Ind2HeightUntilShrinkValue
        {
            get { return Ind2HeightUntilShrink; }
            set { Ind2HeightUntilShrink = value; }
        }
        public static double Ind3HeightOfContainerValue
        {
            get { return Ind3HeightOfContainer; }
            set { Ind3HeightOfContainer = value; }
        }
        public static double Ind3HeightUntilShrinkValue
        {
            get { return Ind3HeightUntilShrink; }
            set { Ind3HeightUntilShrink = value; }
        }
        public static double Ind4HeightOfContainerValue
        {
            get { return Ind4HeightOfContainer; }
            set { Ind4HeightOfContainer = value; }
        }
        public static double Ind4HeightUntilShrinkValue
        {
            get { return Ind4HeightUntilShrink; }
            set { Ind4HeightUntilShrink = value; }
        }
        public static double Ind5HeightOfContainerValue
        {
            get { return Ind5HeightOfContainer; }
            set { Ind5HeightOfContainer = value; }
        }
        public static double Ind5HeightUntilShrinkValue
        {
            get { return Ind5HeightUntilShrink; }
            set { Ind5HeightUntilShrink = value; }
        }
        public static double HeightUntilShrinkValue
        {
            get { return HeightUntilShrink; }
            set { HeightUntilShrink = value; }
        }

        public static bool Indent1IsSelectedValue
        {
            get { return Indent1IsSelected; }
            set { Indent1IsSelected = value; }
        }
        public static bool Indent2IsSelectedValue
        {
            get { return Indent2IsSelected; }
            set { Indent2IsSelected = value; }
        }
        public static bool Indent3IsSelectedValue
        {
            get { return Indent3IsSelected; }
            set { Indent3IsSelected = value; }
        }
        public static bool Indent4IsSelectedValue
        {
            get { return Indent4IsSelected; }
            set { Indent4IsSelected = value; }
        }
       public static bool Indent5IsSelectedValue
        {
            get { return Indent5IsSelected; }
            set { Indent5IsSelected = value; }
        }
    
        public static string NameValue
        {
            get { return Name; }
            set { Name = value; }
        }
        public static bool EnabledFlagValue
        {
            get { return EnabledFlag; }
            set { EnabledFlag = value; }
        }
        public static int ResultValueValue
        {
            get { return ResultValue; }
            set { ResultValue = value; }
        }
        
   public static int Ind1ResultValueValue
        {
            get { return Ind1ResultValue; }
            set { Ind1ResultValue = value; }
        }
        public static int Ind2ResultValueValue
        {
            get { return Ind2ResultValue; }
            set { Ind2ResultValue = value; }
        }
        public static int Ind3ResultValueValue
        {
            get { return Ind3ResultValue; }
            set { Ind3ResultValue = value; }
        }
        public static int Ind4ResultValueValue
        {
            get { return Ind4ResultValue; }
            set { Ind4ResultValue = value; }
        }
        public static int Ind5ResultValueValue
        {
            get { return Ind5ResultValue; }
            set { Ind5ResultValue = value; }
        }
        public static int TintValueValue
        {
            get { return TintValue; }
            set { TintValue = value; }
        }

        public static int Ind1TintValueValue
        {
            get { return Ind1TintValue; }
            set { Ind1TintValue = value; }
        }

        public static int Ind2TintValueValue
        {
            get { return Ind2TintValue; }
            set { Ind2TintValue = value; }
        }

        public static int Ind3TintValueValue
        {
            get { return Ind3TintValue; }
            set { Ind3TintValue = value; }
        }

        public static int Ind4TintValueValue
        {
            get { return Ind4TintValue; }
            set { Ind4TintValue = value; }
        }

        public static int Ind5TintValueValue
        {
            get { return Ind5TintValue; }
            set { Ind5TintValue = value; }
        }
        
        public static int StartLocationValue
        {
            get { return StartLocation; }
            set { StartLocation = value; }
        }
        public static int Ind1StartLocationValue
        {
            get { return Ind1StartLocation; }
            set { Ind1StartLocation = value; }
        }
        public static int Ind2StartLocationValue
        {
            get { return Ind2StartLocation; }
            set { Ind2StartLocation = value; }
        }
        public static int Ind3StartLocationValue
        {
            get { return Ind3StartLocation; }
            set { Ind3StartLocation = value; }
        }
        public static int Ind4StartLocationValue
        {
            get { return Ind4StartLocation; }
            set { Ind4StartLocation = value; }
        }
        public static int Ind5StartLocationValue
        {
            get { return Ind5StartLocation; }
            set { Ind5StartLocation = value; }
        }
        public static string UnitsSelectedValue
        {
            get { return UnitsSelected; }
            set { UnitsSelected = value; }
        }
        public static string ConfigurationFileValue
        {
            get { return ConfigurationFile; }
            set { ConfigurationFile = value; }
        }

        public static string ReleasedToValue
        {
            get { return ReleasedTo; }
            set { ReleasedTo = value; }
        }
        

    }
}
