﻿using System;
using System.Collections.Generic;
using System;

using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace StretchApp.Parameters
{
    public class TupleKeeper
    {
        // List<Tuple<int, int>> listTint = new List<Tuple<int, int>>();


        private static List<int> ListTint = new List<int>();
        private static int ElementAt;
        private int[] TintArray = new int[69];

        public static void CreateList()
        {
            ListTint.Add(0);
            ListTint.Add(0);
            ListTint.Add(1);
            ListTint.Add(1);
            ListTint.Add(2);
            ListTint.Add(2);
            ListTint.Add(2);
            ListTint.Add(3);
            ListTint.Add(3);
            ListTint.Add(4);
            ListTint.Add(4);
            ListTint.Add(4);
            ListTint.Add(5);
            ListTint.Add(5);
            ListTint.Add(6);
            ListTint.Add(6);
            ListTint.Add(6);
            ListTint.Add(7);
            ListTint.Add(7);
            ListTint.Add(8);
            ListTint.Add(8);
            ListTint.Add(8);
            ListTint.Add(9);
            ListTint.Add(9);
            ListTint.Add(10);
            ListTint.Add(10);
            ListTint.Add(10);
            ListTint.Add(11);
            ListTint.Add(11);
            ListTint.Add(12);
            ListTint.Add(12);
            ListTint.Add(12);
            ListTint.Add(13);
            ListTint.Add(13);
            ListTint.Add(14);
            ListTint.Add(14);
            ListTint.Add(14);
            ListTint.Add(15);
            ListTint.Add(15);
            ListTint.Add(16);
            ListTint.Add(16);
            ListTint.Add(16);
            ListTint.Add(17);
            ListTint.Add(17);
            ListTint.Add(17);
            ListTint.Add(18);
            ListTint.Add(18);
            ListTint.Add(18);
            ListTint.Add(18);
            ListTint.Add(19);
            ListTint.Add(19);
            ListTint.Add(20);
            ListTint.Add(20);
            ListTint.Add(20);
            ListTint.Add(13);
            ListTint.Add(21);
            ListTint.Add(21);
            ListTint.Add(21);
            ListTint.Add(21);
            ListTint.Add(22);
            ListTint.Add(22);
            ListTint.Add(22);
            ListTint.Add(23);
            ListTint.Add(23);
            ListTint.Add(23);
            ListTint.Add(24);
            ListTint.Add(24);
            ListTint.Add(24);
            ListTint.Add(24);
            ListTint.Add(25);
            ListTint.Add(25);
            ListTint.Add(25);
            ListTint.Add(26);
            ListTint.Add(26);
            ListTint.Add(26);
            ListTint.Add(27);
            ListTint.Add(27);
            ListTint.Add(27);
            ListTint.Add(27);
            ListTint.Add(28);
            ListTint.Add(28);
            ListTint.Add(22);
            ListTint.Add(22);

        }
        
        public static int ElementAtValue
        {
            get { return ElementAt; }
            set { ElementAt = value; }
        }

        public static int GetlistElement()
        {
            return ListTint[ElementAt];
        }

        //public void CreateTintList()
        //{
        //    listTint.Add(new Tuple<int, int>(0, 0));
        //    listTint.Add(new Tuple<int, int>(1, 0));
        //    listTint.Add(new Tuple<int, int>(2, 0));
        //    listTint.Add(new Tuple<int, int>(3, 0));
        //    listTint.Add(new Tuple<int, int>(4, 0));
        //    listTint.Add(new Tuple<int, int>(5, 0));
        //    listTint.Add(new Tuple<int, int>(6, 0));
        //    listTint.Add(new Tuple<int, int>(7, 0));
        //    listTint.Add(new Tuple<int, int>(8, 0));
        //    listTint.Add(new Tuple<int, int>(9, 0));
        //    listTint.Add(new Tuple<int, int>(12, 0));
        //    listTint.Add(new Tuple<int, int>(11, 0));
        //    listTint.Add(new Tuple<int, int>(12, 0));
        //    listTint.Add(new Tuple<int, int>(13, 0));
        //    listTint.Add(new Tuple<int, int>(14, 0));
        //    listTint.Add(new Tuple<int, int>(15, 0));
        //    listTint.Add(new Tuple<int, int>(16, 0));
        //    listTint.Add(new Tuple<int, int>(17, 0));
        //    listTint.Add(new Tuple<int, int>(18, 0));
        //    listTint.Add(new Tuple<int, int>(19, 0));
        //    listTint.Add(new Tuple<int, int>(20, 0));
        //    listTint.Add(new Tuple<int, int>(21, 0));
        //    listTint.Add(new Tuple<int, int>(22, 0));
        //    listTint.Add(new Tuple<int, int>(23, 0));
        //    listTint.Add(new Tuple<int, int>(25, 0));
        //    listTint.Add(new Tuple<int, int>(26, 0));
        //    listTint.Add(new Tuple<int, int>(27, 0));
        //    listTint.Add(new Tuple<int, int>(28, 0));
        //    listTint.Add(new Tuple<int, int>(29, 0));
        //    listTint.Add(new Tuple<int, int>(30, 0));
        //    listTint.Add(new Tuple<int, int>(31, 0));
        //    listTint.Add(new Tuple<int, int>(32, 0));
        //    listTint.Add(new Tuple<int, int>(33, 0));
        //    listTint.Add(new Tuple<int, int>(34, 0));
        //    listTint.Add(new Tuple<int, int>(35, 0));
        //    listTint.Add(new Tuple<int, int>(36, 0));
        //    listTint.Add(new Tuple<int, int>(37, 0));
        //    listTint.Add(new Tuple<int, int>(38, 0));
        //    listTint.Add(new Tuple<int, int>(39, 0));
        //    listTint.Add(new Tuple<int, int>(40, 0));
        //    listTint.Add(new Tuple<int, int>(41, 0));
        //    listTint.Add(new Tuple<int, int>(42, 0));
        //    listTint.Add(new Tuple<int, int>(43, 0));
        //    listTint.Add(new Tuple<int, int>(44, 0));
        //    listTint.Add(new Tuple<int, int>(45, 0));
        //    listTint.Add(new Tuple<int, int>(46, 0));
        //    listTint.Add(new Tuple<int, int>(47, 0));
        //    listTint.Add(new Tuple<int, int>(48, 0));
        //    listTint.Add(new Tuple<int, int>(49, 0));
        //    listTint.Add(new Tuple<int, int>(50, 0));
        //    listTint.Add(new Tuple<int, int>(51, 0));
        //    listTint.Add(new Tuple<int, int>(52, 0));
        //    listTint.Add(new Tuple<int, int>(53, 0));
        //    listTint.Add(new Tuple<int, int>(54, 0));
        //    listTint.Add(new Tuple<int, int>(55, 0));
        //    listTint.Add(new Tuple<int, int>(56, 0));
        //    listTint.Add(new Tuple<int, int>(57, 0));
        //    listTint.Add(new Tuple<int, int>(58, 0));
        //    listTint.Add(new Tuple<int, int>(59, 0));
        //    listTint.Add(new Tuple<int, int>(60, 0));
        //    listTint.Add(new Tuple<int, int>(61, 0));
        //    listTint.Add(new Tuple<int, int>(62, 0));
        //    listTint.Add(new Tuple<int, int>(63, 0));
        //    listTint.Add(new Tuple<int, int>(64, 0));
        //    listTint.Add(new Tuple<int, int>(65, 0));
        //    listTint.Add(new Tuple<int, int>(66, 0));
        //    listTint.Add(new Tuple<int, int>(67, 0));
        //    listTint.Add(new Tuple<int, int>(68, 0));
        //    listTint.Add(new Tuple<int, int>(69, 0));
        //    listTint.Add(new Tuple<int, int>(70, 0));
        //    listTint.Add(new Tuple<int, int>(71, 0));
        //    listTint.Add(new Tuple<int, int>(72, 0));
        //    listTint.Add(new Tuple<int, int>(73, 0));
        //    listTint.Add(new Tuple<int, int>(74, 0));
        //    listTint.Add(new Tuple<int, int>(75, 0));
        //    listTint.Add(new Tuple<int, int>(76, 0));
        //    listTint.Add(new Tuple<int, int>(77, 0));
        //    listTint.Add(new Tuple<int, int>(78, 0));
        //    listTint.Add(new Tuple<int, int>(79, 0));
        //    listTint.Add(new Tuple<int, int>(80, 0));


      //  }


        //public void CreateTuple()
        //{
        //    Tuple<int, string, bool> tuple =
        //        new Tuple<int, string, bool>(1, "cat", true);
        //}
  //      var tupleList = new List<(int, string)>
  //{
  //    (1, "cow"),
  //    (5, "chickens"),
  //    (1, "airplane")
  //};

    }
}
