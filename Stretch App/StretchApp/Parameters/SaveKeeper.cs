﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace StretchApp.Parameters
{
    public class SaveKeeper
    {
        private static string SaveName;
        private static List<string> ConfigurationList;

        public static string SaveNameValue
        {
            get { return SaveName; }
            set { SaveName = value; }
        }

        public static List<string> ConfigudationListValue
        {
            get { return ConfigurationList; }

        }

        public static void CreateConfigurationList()
        {
            ConfigurationList = new List<string>();
        }

        public static void AddToConfigurationList(string input)
        {
            ConfigurationList.Add(input);
        }
    }
}
