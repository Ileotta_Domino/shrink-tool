﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace StretchApp.Parameters.Interfaces
{
    public interface ITupleProcessor
    {
        void InitialiseTuple();
        int GetElementAt(int input);

    }
}
