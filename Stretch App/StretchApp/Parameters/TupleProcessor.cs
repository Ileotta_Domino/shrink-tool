﻿using StretchApp.Parameters.Interfaces;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace StretchApp.Parameters
{
    public class TupleProcessor: ITupleProcessor
    {
        public void InitialiseTuple()
        {
            TupleKeeper.CreateList();
        }

        public int GetElementAt(int input)
        {
            TupleKeeper.ElementAtValue = input;
            return TupleKeeper.GetlistElement();
        }

    }
}
