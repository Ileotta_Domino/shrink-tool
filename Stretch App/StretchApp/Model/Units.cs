﻿using StretchApp.Parameters;
using StretchApp.Parameters.Interfaces;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace StretchApp.Model
{
    public class Units : INotifyPropertyChanged
    {
        private readonly IinputProcessor _inputprocessor;

        private readonly string _value;
        string _UnitsName;

        public Units( IinputProcessor iinputProcessor)
        {
            _inputprocessor = iinputProcessor;
        }

        public override string ToString()
        {
            return _value;
        }

        public string UnitsName
        {
            get { return _UnitsName; }
            set
            {
                if (_UnitsName != value)
                {
                    _UnitsName = value;
                    _inputprocessor.SetUnits(value);
                    OnPropertyChanged("UnitsName");
                }
            }
        }

        private void OnPropertyChanged(string prop)
        {
            if (PropertyChanged != null)
            { PropertyChanged(this, new PropertyChangedEventArgs(prop)); }
        }
        public event PropertyChangedEventHandler PropertyChanged;
    }
}
