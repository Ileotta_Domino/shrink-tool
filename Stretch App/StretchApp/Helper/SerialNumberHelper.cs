﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Management;
using StretchApp.Helper.Interfaces;

namespace StretchApp.Helper
{
    public class SerialNumberHelper: ISerialNumberHelper
    {
        public string TakeSerialNumber()
        {
            string serialnumber="";

            ManagementObjectSearcher MOS = new ManagementObjectSearcher("Select * From Win32_BaseBoard");
            foreach (ManagementObject getserial in MOS.Get())
            {
                // label1.Text = "Your motherboard serial is : " + getserial["SerialNumber"].ToString();
                serialnumber = getserial["SerialNumber"].ToString();
            }
            return serialnumber;

        }
    }
}
