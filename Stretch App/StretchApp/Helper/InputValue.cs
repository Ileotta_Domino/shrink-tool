﻿using StretchApp.Parameters.Interfaces;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace StretchApp.Helper
{
   public  class InputValue: INotifyPropertyChanged
    {
        int _InputValue;
        int _inputValueToKeep;
        private readonly IinputProcessor _inputValueProcessor;


        public InputValue(IinputProcessor inputValueProcessor)
        {
            _inputValueProcessor = inputValueProcessor;
        }
        public int InputValueToShow
        {
            get { return _InputValue; }

            set
            {
                if (_InputValue != value)
                {
                    _InputValue = value;
                    RaisePropertyChanged("InputValueToShow");
                }
            }
        }


        public int InputValueToKeep
        {
            get
            {
                    return _inputValueToKeep = _inputValueProcessor.GetInput1() ;
            }

            set
            {
                if (_inputValueToKeep != value)
                {
                    _inputValueProcessor.SetInput1(value);
                    RaisePropertyChanged("InputValueToShow");
                }
            }
        }

        private void RaisePropertyChanged(string prop)
        {
            if (PropertyChanged != null)
            { PropertyChanged(this, new PropertyChangedEventArgs(prop)); }
        }
        public event PropertyChangedEventHandler PropertyChanged;
    }
}
