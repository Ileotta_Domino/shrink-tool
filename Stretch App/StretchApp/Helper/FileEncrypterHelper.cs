﻿using StretchApp.Helper.Interfaces;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Security.Cryptography;
using System.Text;
using System.Threading.Tasks;

namespace StretchApp.Helper
{

    public class FileEncrypterHelper: IFileEncrypterHelper
    {
        private readonly ISerialNumberHelper _serialNumberHelper=new SerialNumberHelper();

        private const int Keysize = 256;

        // This constant determines the number of iterations for the password bytes generation function.
        private const int DerivationIterations = 1000;
        public FileEncrypterHelper()
        {
        }


        public void WritePwdToFile(string input, string path)
        {

            string createText = input + Environment.NewLine;
            File.WriteAllText(path, createText);
        }


        public void EncryptFile(string filename)
        {

            try
            {
              //  string FileName = "test.xml";

    //              Console.WriteLine("Encrypt " + filename);

                // Encrypt the file.
                AddEncryption(filename);

          //      Console.WriteLine("Decrypt " + filename);

                // Decrypt the file.
           //     RemoveEncryption(filename);

        //        Console.WriteLine("Done");
            }
            catch (Exception e)
            {
                Console.WriteLine(e);
            }

            Console.ReadLine();
        }

        public  void AddEncryption(string FileName)
        {

            File.Encrypt(FileName);

        }

        public string ReadEncrline(string FileName)
        {
            string templine = "";
            RemoveEncryption(FileName);
            templine = File.ReadLines(FileName).First();
            AddEncryption(FileName);
            return templine;
        }

        public  void RemoveEncryption(string FileName)
        {
            File.Decrypt(FileName);
        }

        public string  PwdFileDecrypter()
        {
       //     RemoveEncryption(@"c:\encrypt\hlp.txt");
         //   File.Create(@"c:\Encrypt\temp.txt");
           string line= Decrypt( File.ReadLines(@"c:\Domino\Shrink\Key\Pwh.txt").First(), _serialNumberHelper.TakeSerialNumber());
           // string line1 = File.ReadLines(@"c:\Encrypt\temp.txt").First();
            return line;
        }

        public string PwdFileSerDecrypter()
        {

            string line = Decrypt(File.ReadLines(@"c:\Domino\Shrink\Key\Pwh.txt").First(), _serialNumberHelper.TakeSerialNumber());
            // string line1 = File.ReadLines(@"c:\Encrypt\temp.txt").First();
            return line;
        }

        public string ReadPwdFile()
        {
            string line1 = File.ReadLines(@"c:\Domino\Shrink\Key\temp.txt").First();
            File.Delete(@"c:\Domino\Shrink\Key\temp.txt");
            return line1;
        }

        public void PwdFileEncrypter()
        {

            //  File.Create(@"c:\encrypt\temp.txt");

            File.WriteAllText(@"c:\Domino\Shrink\Key\Pwh.txt",  Encrypt(File.ReadLines(@"c:\Domino\Shrink\Key\temp.txt").First(), _serialNumberHelper.TakeSerialNumber()));

            File.Delete(@"c:\Domino\Shrink\Key\temp.txt");

        }


        public void PwdFileWithCodeEncrypter()
        {

            //  File.Create(@"c:\encrypt\temp.txt");

            File.WriteAllText(@"c:\Domino\Shrink\Key\Pwh.txt", Encrypt(File.ReadLines(@"c:\Domino\Shrink\encrypt\temp.txt").First(), @"c:\Decrypt\CodeTemp.txt"));

            File.Delete(@"c:\Domino\Shrink\Key\temp.txt");

        }

        public void CodeFileEncrypter()
        {

            //  File.Create(@"c:\encrypt\temp.txt");

            File.WriteAllText(@"c:\Domino\Shrink\Key\Pwh.txt", Encrypt(File.ReadLines(@"c:\Domino\Shrink\encrypt\temp.txt").First(), _serialNumberHelper.TakeSerialNumber()));

            File.Delete(@"c:\Domino\Shrink\Key\temp.txt");

        }

        private string Encrypt(string plainText, string passPhrase)
        {
            // Salt and IV is randomly generated each time, but is preprended to encrypted cipher text
            // so that the same Salt and IV values can be used when decrypting.  
            var saltStringBytes = Generate256BitsOfRandomEntropy();
            var ivStringBytes = Generate256BitsOfRandomEntropy();
            var plainTextBytes = Encoding.UTF8.GetBytes(plainText);
            using (var password = new Rfc2898DeriveBytes(passPhrase, saltStringBytes, DerivationIterations))
            {
                var keyBytes = password.GetBytes(Keysize / 8);
                using (var symmetricKey = new RijndaelManaged())
                {
                    symmetricKey.BlockSize = 256;
                    symmetricKey.Mode = CipherMode.CBC;
                    symmetricKey.Padding = PaddingMode.PKCS7;
                    using (var encryptor = symmetricKey.CreateEncryptor(keyBytes, ivStringBytes))
                    {
                        using (var memoryStream = new MemoryStream())
                        {
                            using (var cryptoStream = new CryptoStream(memoryStream, encryptor, CryptoStreamMode.Write))
                            {
                                cryptoStream.Write(plainTextBytes, 0, plainTextBytes.Length);
                                cryptoStream.FlushFinalBlock();
                                // Create the final bytes as a concatenation of the random salt bytes, the random iv bytes and the cipher bytes.
                                var cipherTextBytes = saltStringBytes;
                                cipherTextBytes = cipherTextBytes.Concat(ivStringBytes).ToArray();
                                cipherTextBytes = cipherTextBytes.Concat(memoryStream.ToArray()).ToArray();
                                memoryStream.Close();
                                cryptoStream.Close();
                                return Convert.ToBase64String(cipherTextBytes);
                            }
                        }
                    }
                }
            }
        }

       private string Decrypt(string cipherText, string passPhrase)
        {
            // Get the complete stream of bytes that represent:
            // [32 bytes of Salt] + [32 bytes of IV] + [n bytes of CipherText]
            var cipherTextBytesWithSaltAndIv = Convert.FromBase64String(cipherText);
            // Get the saltbytes by extracting the first 32 bytes from the supplied cipherText bytes.
            var saltStringBytes = cipherTextBytesWithSaltAndIv.Take(Keysize / 8).ToArray();
            // Get the IV bytes by extracting the next 32 bytes from the supplied cipherText bytes.
            var ivStringBytes = cipherTextBytesWithSaltAndIv.Skip(Keysize / 8).Take(Keysize / 8).ToArray();
            // Get the actual cipher text bytes by removing the first 64 bytes from the cipherText string.
            var cipherTextBytes = cipherTextBytesWithSaltAndIv.Skip((Keysize / 8) * 2).Take(cipherTextBytesWithSaltAndIv.Length - ((Keysize / 8) * 2)).ToArray();

            using (var password = new Rfc2898DeriveBytes(passPhrase, saltStringBytes, DerivationIterations))
            {
                var keyBytes = password.GetBytes(Keysize / 8);
                using (var symmetricKey = new RijndaelManaged())
                {
                    symmetricKey.BlockSize = 256;
                    symmetricKey.Mode = CipherMode.CBC;
                    symmetricKey.Padding = PaddingMode.PKCS7;
                    using (var decryptor = symmetricKey.CreateDecryptor(keyBytes, ivStringBytes))
                    {
                        using (var memoryStream = new MemoryStream(cipherTextBytes))
                        {
                            using (var cryptoStream = new CryptoStream(memoryStream, decryptor, CryptoStreamMode.Read))
                            {
                                var plainTextBytes = new byte[cipherTextBytes.Length];
                                var decryptedByteCount = cryptoStream.Read(plainTextBytes, 0, plainTextBytes.Length);
                                memoryStream.Close();
                                cryptoStream.Close();
                                return Encoding.UTF8.GetString(plainTextBytes, 0, decryptedByteCount);
                            }
                        }
                    }
                }
            }
        }

        private static byte[] Generate256BitsOfRandomEntropy()
        {
            var randomBytes = new byte[32]; // 32 Bytes will give us 256 bits.
            using (var rngCsp = new RNGCryptoServiceProvider())
            {
                // Fill the array with cryptographically secure random bytes.
                rngCsp.GetBytes(randomBytes);
            }
            return randomBytes;
        }





        
    }
}
