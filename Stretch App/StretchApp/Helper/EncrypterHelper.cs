﻿using StretchApp.Helper.Interfaces;
using StretchApp.Parameters.Interfaces;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Security.Cryptography;
using System.Text;
using System.Threading.Tasks;

namespace StretchApp.Helper
{
   public class EncrypterHelper : IEncrypterHelper
    {
        private readonly ISerialNumberHelper _serialNumberHelper;
        private  readonly IFileEncrypterHelper _fileEncrypterHelper;
        private readonly IinputProcessor _inputProcessor;
        const string EncrFolder = @"c:\Domino\Shrink\Key\";
        const string DecrFolder = @"c:\Domino\Shrink\Decrypt\";
        const string PubKeyFile = @"c:\Domino\Shrink\FromUser\rsaPublicKey.txt";
        
        RSACryptoServiceProvider rsa = new RSACryptoServiceProvider();

        public EncrypterHelper(IFileEncrypterHelper fileEncrypterHelper, ISerialNumberHelper serialNumberHelper, IinputProcessor inputProcessor)
        {
            _fileEncrypterHelper = fileEncrypterHelper;
            _serialNumberHelper = serialNumberHelper;
            _inputProcessor = inputProcessor;
        }

        public void CreateKey()
        {
            CspParameters cspp = new CspParameters();
             string keyName = "Key01";
            string str1;
            string str2;

            cspp.KeyContainerName = keyName;
        //    rsa = new RSACryptoServiceProvider(cspp);
            rsa.PersistKeyInCsp = true;
            if (rsa.PublicOnly == true)
                str1 = "Key: " + cspp.KeyContainerName + " - Public Only";
            else
                str2 = "Key: " + cspp.KeyContainerName + " - Full Key Pair";
        }

        public void ExportPublicKey()
        {
            Directory.CreateDirectory(@"c:\Domino\Shrink\FromUser\");
            StreamWriter sw = new StreamWriter(PubKeyFile, false);
            sw.Write(rsa.ToXmlString(false));
            sw.Close();
        }

        public bool Decrypter(string FileName)
        {
            bool res = false;
            string fName = FileName;
            if (fName != null)
            {
                FileInfo fi = new FileInfo(fName);
                string name = fi.Name;
                res= DecryptFile(name);
            }
            return res;
        }




        public bool  DecryptFileCode(string inFile)
        {
            string tempFilePath= @"c:\Domino\Shrink\Decrypt\CodeTemp.txt";
            // Create instance of Rijndael for
            // symetric decryption of the data.
            RijndaelManaged rjndl = new RijndaelManaged();
            rjndl.KeySize = 256;
            rjndl.BlockSize = 256;
            rjndl.Mode = CipherMode.CBC;

            // Create byte arrays to get the length of
            // the encrypted key and IV.
            // These values were stored as 4 bytes each
            // at the beginning of the encrypted package.
            byte[] LenK = new byte[4];
            byte[] LenIV = new byte[4];

            // Consruct the file name for the decrypted file.
            string outFile = DecrFolder + inFile.Substring(0, inFile.LastIndexOf(".")) + ".txt";

            // Use FileStream objects to read the encrypted
            // file (inFs) and save the decrypted file (outFs).
            using (FileStream inFs = new FileStream(EncrFolder + inFile, FileMode.Open))
            {

                inFs.Seek(0, SeekOrigin.Begin);
                inFs.Seek(0, SeekOrigin.Begin);
                inFs.Read(LenK, 0, 3);
                inFs.Seek(4, SeekOrigin.Begin);
                inFs.Read(LenIV, 0, 3);

                // Convert the lengths to integer values.
                int lenK = BitConverter.ToInt32(LenK, 0);
                int lenIV = BitConverter.ToInt32(LenIV, 0);

                // Determine the start postition of
                // the ciphter text (startC)
                // and its length(lenC).
                int startC = lenK + lenIV + 8;
                int lenC = (int)inFs.Length - startC;

                // Create the byte arrays for
                // the encrypted Rijndael key,
                // the IV, and the cipher text.
                byte[] KeyEncrypted = new byte[lenK];
                byte[] IV = new byte[lenIV];

                // Extract the key and IV
                // starting from index 8
                // after the length values.
                inFs.Seek(8, SeekOrigin.Begin);
                inFs.Read(KeyEncrypted, 0, lenK);
                inFs.Seek(8 + lenK, SeekOrigin.Begin);
                inFs.Read(IV, 0, lenIV);
                Directory.CreateDirectory(DecrFolder);
                // Use RSACryptoServiceProvider
                // to decrypt the Rijndael key.
                byte[] KeyDecrypted = rsa.Decrypt(KeyEncrypted, false);

                // Decrypt the key.
                ICryptoTransform transform = rjndl.CreateDecryptor(KeyDecrypted, IV);

                // Decrypt the cipher text from
                // from the FileSteam of the encrypted
                // file (inFs) into the FileStream
                // for the decrypted file (outFs).
                using (FileStream outFs = new FileStream(outFile, FileMode.Create))
                {

                    int count = 0;
                    int offset = 0;

                    // blockSizeBytes can be any arbitrary size.
                    int blockSizeBytes = rjndl.BlockSize / 8;
                    byte[] data = new byte[blockSizeBytes];


                    // By decrypting a chunk a time,
                    // you can save memory and
                    // accommodate large files.

                    // Start at the beginning
                    // of the cipher text.
                    inFs.Seek(startC, SeekOrigin.Begin);
                    using (CryptoStream outStreamDecrypted = new CryptoStream(outFs, transform, CryptoStreamMode.Write))
                    {
                        do
                        {
                            count = inFs.Read(data, 0, blockSizeBytes);
                            offset += count;
                            outStreamDecrypted.Write(data, 0, count);

                        }
                        while (count > 0);

                        outStreamDecrypted.FlushFinalBlock();
                        outStreamDecrypted.Close();
                    }



                    outFs.Close();

                }
                inFs.Close();
            }


      //      using (System.IO.FileStream fs = System.IO.File.Create(tempFilePath))
       //     {
                File.Copy(outFile, tempFilePath, true);
            //    }


            return   CheckCodeMatch(outFile);
        }



        public bool DecryptFile(string inFile)
        {

            // Create instance of Rijndael for
            // symetric decryption of the data.
            RijndaelManaged rjndl = new RijndaelManaged();
            rjndl.KeySize = 256;
            rjndl.BlockSize = 256;
            rjndl.Mode = CipherMode.CBC;

            // Create byte arrays to get the length of
            // the encrypted key and IV.
            // These values were stored as 4 bytes each
            // at the beginning of the encrypted package.
            byte[] LenK = new byte[4];
            byte[] LenIV = new byte[4];

            // Consruct the file name for the decrypted file.
            string outFile = DecrFolder + inFile.Substring(0, inFile.LastIndexOf(".")) + ".txt";

            // Use FileStream objects to read the encrypted
            // file (inFs) and save the decrypted file (outFs).
            using (FileStream inFs = new FileStream(EncrFolder + inFile, FileMode.Open))
            {

                inFs.Seek(0, SeekOrigin.Begin);
                inFs.Seek(0, SeekOrigin.Begin);
                inFs.Read(LenK, 0, 3);
                inFs.Seek(4, SeekOrigin.Begin);
                inFs.Read(LenIV, 0, 3);

                // Convert the lengths to integer values.
                int lenK = BitConverter.ToInt32(LenK, 0);
                int lenIV = BitConverter.ToInt32(LenIV, 0);

                // Determine the start postition of
                // the ciphter text (startC)
                // and its length(lenC).
                int startC = lenK + lenIV + 8;
                int lenC = (int)inFs.Length - startC;

                // Create the byte arrays for
                // the encrypted Rijndael key,
                // the IV, and the cipher text.
                byte[] KeyEncrypted = new byte[lenK];
                byte[] IV = new byte[lenIV];

                // Extract the key and IV
                // starting from index 8
                // after the length values.
                inFs.Seek(8, SeekOrigin.Begin);
                inFs.Read(KeyEncrypted, 0, lenK);
                inFs.Seek(8 + lenK, SeekOrigin.Begin);
                inFs.Read(IV, 0, lenIV);
                Directory.CreateDirectory(DecrFolder);
                // Use RSACryptoServiceProvider
                // to decrypt the Rijndael key.
                byte[] KeyDecrypted = rsa.Decrypt(KeyEncrypted, false);

                // Decrypt the key.
                ICryptoTransform transform = rjndl.CreateDecryptor(KeyDecrypted, IV);

                // Decrypt the cipher text from
                // from the FileSteam of the encrypted
                // file (inFs) into the FileStream
                // for the decrypted file (outFs).
                using (FileStream outFs = new FileStream(outFile, FileMode.Create))
                {

                    int count = 0;
                    int offset = 0;

                    // blockSizeBytes can be any arbitrary size.
                    int blockSizeBytes = rjndl.BlockSize / 8;
                    byte[] data = new byte[blockSizeBytes];


                    // By decrypting a chunk a time,
                    // you can save memory and
                    // accommodate large files.

                    // Start at the beginning
                    // of the cipher text.
                    inFs.Seek(startC, SeekOrigin.Begin);
                    using (CryptoStream outStreamDecrypted = new CryptoStream(outFs, transform, CryptoStreamMode.Write))
                    {
                        do
                        {
                            count = inFs.Read(data, 0, blockSizeBytes);
                            offset += count;
                            outStreamDecrypted.Write(data, 0, count);

                        }
                        while (count > 0);

                        outStreamDecrypted.FlushFinalBlock();
                        outStreamDecrypted.Close();
                    }


                   
                    outFs.Close();
                }
                inFs.Close();
            }
            return CheckCodeMatch(outFile); 
        }

        private bool CheckCodeMatch(string infile)
        {
            bool res = false;

            //    string line1 = File.ReadLines(infile).First();

       
            try
            {
                if ((new FileInfo(@"c:\Domino\Shrink\AppFiles\SavedData.txt").Attributes & FileAttributes.Hidden) == FileAttributes.Hidden)
                {

                    // Get file info
                    FileInfo myFile = new FileInfo(@"c:\Domino\Shrink\AppFiles\SavedData.txt");

                    // Remove the hidden attribute of the file
                    myFile.Attributes &= ~FileAttributes.Hidden;

                    res = (File.ReadLines(infile).First() == File.ReadLines(@"c:\Domino\Shrink\AppFiles\SavedData.txt").First());
                    myFile.Attributes |= FileAttributes.Hidden;
                }
                else
                {
                    res = (File.ReadLines(infile).First() == File.ReadLines(@"c:\Domino\Shrink\AppFiles\SavedData.txt").First());
                }
            }
            catch (FileNotFoundException)
            {
                System.Windows.MessageBox.Show("Please execute the Initialisation process first  ");
            }

            return res;
        }


       public void  AddCode()
        {
            UTF8Encoding utf8 = new UTF8Encoding();
            string strtoencode = _serialNumberHelper.TakeSerialNumber() + _inputProcessor.GetName();
            byte[] encodedBytes = utf8.GetBytes(strtoencode);
            string str1 = System.Text.Encoding.Default.GetString(encodedBytes);
            string path = @"c:\Domino\Shrink\AppFiles\"; // or whatever 
            if (!File.Exists(@"c:\Domino\Shrink\AppFiles\SavedData.txt"))
            {


                //    myFile.Attributes &= ~FileAttributes.Hidden;
                //   File.WriteAllText(path, String.Empty);
                using (StreamWriter twhid = File.CreateText(@"c:\Domino\Shrink\AppFiles\SavedData.txt"))

                twhid.WriteLine(str1);


                FileInfo myFile = new FileInfo(@"c:\Domino\Shrink\AppFiles\SavedData.txt");
                myFile.Attributes |= FileAttributes.Hidden;
            }
            else
            {
                FileInfo myFile = new FileInfo(@"c:\Domino\Shrink\AppFiles\SavedData.txt");
                myFile.Attributes &= ~FileAttributes.Hidden;
                File.WriteAllText(path, String.Empty);
                using (StreamWriter twhid = File.CreateText(@"c:\Domino\Shrink\AppFiles\SavedData.txt"))
                    //       twhid.WriteLine(_serialNumberHelper.TakeSerialNumber() + _inputProcessor.GetName());
                    twhid.WriteLine(str1);
                myFile.Attributes |= FileAttributes.Hidden;
            }


            if (!Directory.Exists(path))
                {
                DirectoryInfo di = Directory.CreateDirectory(@"c:\Domino\Shrink\AppFiles\");
                using (StreamWriter twhid = File.CreateText(@"c:\Domino\Shrink\AppFiles\SavedData.txt"))
                {
                    //twhid.WriteLine(_serialNumberHelper.TakeSerialNumber()+ _inputProcessor.GetName());
                    twhid.WriteLine(str1);
                }

                di.Attributes = FileAttributes.Directory | FileAttributes.Hidden;
                File.SetAttributes(@"c:\Domino\Shrink\AppFiles\SavedData.txt", File.GetAttributes(@"c:\Domino\Shrink\AppFiles\SavedData.txt") | FileAttributes.Hidden);
            }

            if (!Directory.Exists(@"c:\Domino\Shrink\DominoToEncrypt\"))
            {
                DirectoryInfo den = Directory.CreateDirectory(@"c:\Domino\Shrink\DominoToEncrypt\");
            }
                using (StreamWriter twhden = File.CreateText(@"c:\Domino\Shrink\DominoToEncrypt\PwdToEncr.txt"))
                {
             //   twhden.WriteLine(_serialNumberHelper.TakeSerialNumber() + _inputProcessor.GetName());
                twhden.WriteLine(str1);
            }

            

        }

    }
}