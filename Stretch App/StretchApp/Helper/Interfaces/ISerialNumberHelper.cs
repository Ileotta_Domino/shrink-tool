﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace StretchApp.Helper.Interfaces
{
   public interface ISerialNumberHelper
    {
        string TakeSerialNumber();
         
    }
}
