﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace StretchApp.Helper.Interfaces
{
    public interface IEncrypterHelper
    {
        void CreateKey();
        void ExportPublicKey();
        bool Decrypter(string FileName);
        bool DecryptFileCode(string inFile);
        void AddCode();
    }
}
