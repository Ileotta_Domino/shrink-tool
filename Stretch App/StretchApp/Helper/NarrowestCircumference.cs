﻿using StretchApp.Parameters;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;

namespace StretchApp.Helper
{
    public class NarrowestCircumference : INotifyPropertyChanged
    {
        double _WidestCircumferenceValue;
        double _WidestCircumferenceToKeep;
        private readonly IInputProcessor _inputProcessor;
        public NarrowestCircumference(IInputProcessor inputProcessor)
        {
            _inputProcessor = inputProcessor;
            if (DesignerProperties.GetIsInDesignMode(new DependencyObject()))
            {
                this._WidestCircumferenceValue = 42;
            }
        }


        public double WidestCircumferenceToShow
        {
            get { return _WidestCircumferenceValue; }
            //  set { _rowNumber = value; OnPropertyChanged("RowValue"); }
            set
            {
                if (_WidestCircumferenceValue != value)
                {
                    _WidestCircumferenceValue = value;
                    RaisePropertyChanged("WidestCircumferenceToShow");
                }
            }
        }


        public double WidestCircumferenceToKeep
        {
            get
            {

                return _WidestCircumferenceToKeep = _inputProcessor.GetWidestCircumference();
            }

            set
            {
                if (_WidestCircumferenceToKeep != value)
                {
                    _inputProcessor.SetWidestCircumference(value);
                    RaisePropertyChanged("WidestCircumferenceToKeep");
                }
            }
        }

        private void RaisePropertyChanged(string prop)
        {
            if (PropertyChanged != null)
            { PropertyChanged(this, new PropertyChangedEventArgs(prop)); }
        }
        public event PropertyChangedEventHandler PropertyChanged;
    }
}
