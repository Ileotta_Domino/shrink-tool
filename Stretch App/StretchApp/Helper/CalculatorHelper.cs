﻿using StretchApp.Helper.Interfaces;
using StretchApp.Parameters.Interfaces;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace StretchApp.Helper
{
    public class CalculatorHelper: ICalculatorHelper
    {
        private readonly IinputProcessor _inptProcessor;
        private readonly ITupleProcessor _tupleProcessor;

        public CalculatorHelper(IinputProcessor inputProcessor, ITupleProcessor tupleProcessor)
        {
            _inptProcessor = inputProcessor;
            _tupleProcessor = tupleProcessor;
        }

        public int CalulculatePercentage (double part, double total)
        {
            double result;
            result = ((part-total) / part) * 100;
            return Convert.ToInt32( result);
        }

        public int CalulculateStartLocationPercentage(double part, double total)
        {
            double result;
            result = (total / part) * 100;
            return Convert.ToInt32(result);
        }
        public int GetTint(int input)
        {
            int res = 0;

            if (input > 80 || input < 0)
            {
                res = 100;
            }
            else {
                _tupleProcessor.InitialiseTuple();
                res= _tupleProcessor.GetElementAt(input);
            }
            return res;
        }

        public string GetTintToShow(int input)
        {
            string res = "";

            if (input > 80 || input < 0)
            {
                res = "N/A";
            }
            else
            {
                res = input.ToString();
            }
            return res;
        }
    }
}
