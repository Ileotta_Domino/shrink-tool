﻿using StretchApp.Parameters.Interfaces;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace StretchApp.Helper
{
    public class IDValue : INotifyPropertyChanged
    {
        string _IDValue;
        string _IDValueToKeep;

        private readonly IinputProcessor _inputValueProcessor;

        public IDValue(IinputProcessor inputValueProcessor)
        {
            _inputValueProcessor = inputValueProcessor;
        }

        public string IDValueToShow
        {
            get { return _IDValue; }

            set
            {
                if (_IDValue != value)
                {
                    _IDValue = value;
                    RaisePropertyChanged("_IDValueToShow");
                }
            }
        }

        public string IDValueToKeep
        {
            get
            {
                return _IDValueToKeep = _inputValueProcessor.GetID();
            }

            set
            {
                if (_IDValueToKeep != value)
                {
                    _inputValueProcessor.SetID(value);
                    RaisePropertyChanged("IDValueToKeep");
                }
            }
        }

        private void RaisePropertyChanged(string prop)
        {
            if (PropertyChanged != null)
            { PropertyChanged(this, new PropertyChangedEventArgs(prop)); }
        }
        public event PropertyChangedEventHandler PropertyChanged;
    }
}
