﻿using Ninject;
using StretchApp.Helper;
using StretchApp.Helper.Interfaces;
using StretchApp.Parameters;
using StretchApp.Parameters.Interfaces;
using StretchApp.ViewModels;
using StretchApp.Views;
using System.IO;
using System.Windows;

namespace StretchApp
{
    /// <summary>
    /// Interaction logic for App.xaml
    /// </summary>
    public partial class App : Application
    {
        private IKernel container;
        private string pwf = @"c:\Domino\Shrink\Key\Pwh.txt";


        protected override void OnStartup(StartupEventArgs e)
        {
            base.OnStartup(e);

            var container = new StandardKernel();

            // Register types
            container.Bind<IEncrypterHelper>().To<EncrypterHelper>();
            container.Bind<ICalculatorHelper>().To<CalculatorHelper>();
            container.Bind<IinputProcessor>().To<IInputProcessor>();
            container.Bind<IFileEncrypterHelper>().To<FileEncrypterHelper>();
            container.Bind<ISerialNumberHelper>().To<SerialNumberHelper>();
            container.Bind<ITupleProcessor>().To<TupleProcessor>();
            container.Bind<IConfigurationListHelper>().To<ConfigurationListHelper>();

            container.Bind<Window>().To<MainWindow>();
            container.Bind<Window>().To<CalculatorPage>();
            container.Bind<Window>().To<IdentifierPage>();
            container.Bind<Window>().To<ConfigurationPage>();
            container.Bind<Window>().To<AboutPage>();
            // Build the application object graph
            //   var window = container.Get<IWindow>();

            // Show the main window.

            Directory.CreateDirectory(@"c:\Domino\Shrink\FromUser\");
            Directory.CreateDirectory(@"c:\Domino\Shrink\Key\");
            Directory.CreateDirectory(@"c:\Domino\Shrink\Decrypt\");
            Directory.CreateDirectory(@"c:\Domino\Shrink\AppFiles\");

            MainWindow mw = new MainWindow();
            mw.DataContext = container.Get<MainViewModel>();
            CalculatorPage vc = new CalculatorPage();
            vc.DataContext=container.Get<ViewModelCalculatorPage>();
            IdentifierPage ic = new IdentifierPage();
            ic.DataContext = container.Get<IdentifierPageViewModel>();
            ConfigurationPage cf = new ConfigurationPage();
            cf.DataContext = container.Get<ViewModelConfigurationPage>();
            AboutPage ab = new AboutPage();
            ab.DataContext = container.Get<ViewModelAboutPage>();
            if (!File.Exists(pwf)){
             ic.Show();
        }
           else { mw.Show(); }
              
        }
        private void ComposeObjects()
        {
            Current.MainWindow = this.container.Get<MainWindow>();
        }

        private void ConfigureContainer()
        {
            this.container = new StandardKernel();
            container.Bind<IEncrypterHelper>().To<EncrypterHelper>();

        }
    }
}
