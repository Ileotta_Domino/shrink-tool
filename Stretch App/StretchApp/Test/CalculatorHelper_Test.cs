﻿using Ninject;
using Ninject.MockingKernel.Moq;
using NUnit.Framework;
using StretchApp.Helper;
using StretchApp.Helper.Interfaces;
using StretchApp.Parameters.Interfaces;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace StretchApp.Test
{
    [TestFixture]
    public class CalculatorHelper_Test
    {
        private readonly MoqMockingKernel _kernel;

        public CalculatorHelper_Test()
        {
            _kernel = new MoqMockingKernel();
            _kernel.Bind<ICalculatorHelper>().To<CalculatorHelper>();
        }
        [SetUp]
        public void SetUp()
        {
            _kernel.Reset();
        }
        //[Test]
        //public void Testone()
        //{
        //    var inputProcessor_Mock = _kernel.GetMock<IinputProcessor>();
        //    var tupleProcessor_Mock = _kernel.GetMock<ITupleProcessor>();

        //    var _calculatorHelper = _kernel.Get<ICalculatorHelper>();
        //    var result = _calculatorHelper.CalulculatePercentage(100, 50);
        //    Assert.AreEqual(result, "50");
        //}
        //[TestCase(100,50,"50")]
        //public void Test1(int part, int total, string res)
        //{
        //    //setup the mock
        //    var inputProcessor_Mock = _kernel.GetMock<IinputProcessor>();
        //    var tupleProcessor_Mock = _kernel.GetMock<ITupleProcessor>();

        //    var _calculatorHelper = _kernel.Get<ICalculatorHelper>();
        //    var result = _calculatorHelper.CalulculatePercentage(part,total);
        //    Assert.AreEqual(result, res);

        //}
    }
}
