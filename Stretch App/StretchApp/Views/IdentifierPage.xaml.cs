﻿using StretchApp.Helper;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace StretchApp.Views
{
    /// <summary>
    /// Interaction logic for IdentifierPage.xaml
    /// </summary>
    public partial class IdentifierPage : Window
    {
        public IdentifierPage()
        {
            InitializeComponent();
            DataContext = this;
            this.MouseLeftButtonDown += delegate { this.DragMove(); };
            DataContextChanged += new DependencyPropertyChangedEventHandler(MainWindow_DataContextChanged);
        }
        void MainWindow_DataContextChanged(object sender, DependencyPropertyChangedEventArgs e)
        {
            var dc = DataContext as IClosableViewModel;
            dc.CloseWindowEvent += new EventHandler(dc_CloseWindowEvent);
        }
        void dc_CloseWindowEvent(object sender, EventArgs e)
        {
            this.Close();
        }

        private void PasswordDesired_TextChanged(object sender, TextChangedEventArgs e)
        {

        }

        private void ConfirmPassword_TextChanged(object sender, TextChangedEventArgs e)
        {

        }

        private void SendPassword_Click(object sender, RoutedEventArgs e)
        {
        }

        private void Name_TextChanged(object sender, TextChangedEventArgs e)
        {

        }

        //public System.Security.SecureString GivenPassword
        //{
        //    get
        //    {
        //        return FirstPassword.SecurePassword;
        //    }
        //}
        //public System.Security.SecureString ConfirmedPassword
        //{
        //    get
        //    {
        //        return ConfirmPassword.SecurePassword;
        //    }
        //}

        private void Button_Click(object sender, RoutedEventArgs e)
        {
            System.Windows.Application.Current.Shutdown();
            Close();
        }

        private void Button_Click_1(object sender, RoutedEventArgs e)
        {

        }

        private void ConfirmPassword_Click(object sender, RoutedEventArgs e)
        {

        }
    }
}
