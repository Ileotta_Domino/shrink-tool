﻿using StretchApp.Helper;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace StretchApp.Views
{
    /// <summary>
    /// Interaction logic for AboutPage.xaml
    /// </summary>
    public partial class AboutPage : Window
    {
        public AboutPage()
        {
            InitializeComponent();
            DataContextChanged += new DependencyPropertyChangedEventHandler(AboutPage_DataContextChanged);
        }
        void AboutPage_DataContextChanged(object sender, DependencyPropertyChangedEventArgs e)
        {
            var dc = DataContext as IClosableViewModel;
            dc.CloseWindowEvent += new EventHandler(dc_CloseWindowEvent);
        }
        void dc_CloseWindowEvent(object sender, EventArgs e)
        {
            this.Close();
        }
    }
}
