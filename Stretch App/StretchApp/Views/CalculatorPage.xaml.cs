﻿using StretchApp.Helper;
using StretchApp.Helper.Interfaces;
using StretchApp.Parameters.Interfaces;
using StretchApp.ViewModels;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace StretchApp.Views
{
    /// <summary>
    /// Interaction logic for CalculatorPage.xaml
    /// </summary>
    public partial class CalculatorPage : Window
    {

        public CalculatorPage()
        {

            InitializeComponent();
            DataContext = this;
            this.MouseLeftButtonDown += delegate { this.DragMove(); };
            DataContextChanged += new DependencyPropertyChangedEventHandler(MainWindow_DataContextChanged);
        }
        void MainWindow_DataContextChanged(object sender, DependencyPropertyChangedEventArgs e)
        {
            var dc = DataContext as IClosableViewModel;
            dc.CloseWindowEvent += new EventHandler(dc_CloseWindowEvent);
        }
        void dc_CloseWindowEvent(object sender, EventArgs e)
        {
            this.Close();
        }

        private void Button_Click(object sender, RoutedEventArgs e)
        {

        }


        private void NarrowestCircumference_TextChanged(object sender, TextChangedEventArgs e)
        {

        }

        private void NarrowestCircumferenceValidationTextBox(object sender, TextCompositionEventArgs e)
        {
            Regex regex = new Regex("[^0-9,.]+");
            e.Handled = regex.IsMatch(e.Text);
        }

        private void WidestCircumference_TextChanged(object sender, TextChangedEventArgs e)
        {

        }

        private void WidestCircumferenceValidationTextBox(object sender, TextCompositionEventArgs e)
        {
            Regex regex = new Regex("[^0-9,.]+");
            e.Handled = regex.IsMatch(e.Text);
        }

        private void Button_Click_1(object sender, RoutedEventArgs e)
        {
            System.Windows.Application.Current.Shutdown();
            Close();
        }

        private void HeightOfContainer_TextChanged(object sender, TextChangedEventArgs e)
        {

        }
        private void HeightOfContainerValidationTextBox(object sender, TextCompositionEventArgs e)
        {
            Regex regex = new Regex("[^0-9,.]+");
            e.Handled = regex.IsMatch(e.Text);
        }

        private void HeightUntilShrink_TextChanged(object sender, TextChangedEventArgs e)
        {

        }
        private void HeightUntilShrinkValidationTextBox(object sender, TextCompositionEventArgs e)
        {
            Regex regex = new Regex("[^0-9,.]+");
            e.Handled = regex.IsMatch(e.Text);
        }

        private void Ind1HeightUntilShrink_TextChanged(object sender, TextChangedEventArgs e)
        {

        }

        private void Ind2HeightUntilShrink_TextChanged(object sender, TextChangedEventArgs e)
        {

        }

        private void Ind3HeightUntilShrink_TextChanged(object sender, TextChangedEventArgs e)
        {

        }

        private void Ind4WidestCircumference_TextChanged(object sender, TextChangedEventArgs e)
        {

        }
    }
}
